source("common.R")

tsvFiles <- dir(file.path(Sys.getenv("DATAFILES_TSV_PATH"),"genes"), pattern="_genes.tsv$", full.names=T,recursive = T)
cat(length(tsvFiles),"*_genes.tsv files found.\n")

# the "genes" model is in the "genes" django app. So the convention is that the table is named "genes_genes"
dbTable = "genes_genes"

con <- salmobase_connect()

dropIfTableExists(con,dbTable)

id_max <- 0 # current max unique id across all genes

for( tsvFile in tsvFiles){
  # load table (all as character)
  tbl <- read.delim(tsvFile,colClasses = "character")
  
  tbl$species <- basename(dirname(dirname(tsvFile)))
  tbl$asm <- basename(dirname(tsvFile))
  tbl$ann <- sub("_genes.tsv$","",basename(tsvFile))
  
  
  # convert numeric columns to int64
  for(column in c("start","end","version","position")){
    tbl[[column]] <- bit64::as.integer64(tbl[[column]])
  }

  # Generate unique ID for each entry in table (note: gene_id is not unique)
  tbl$id <- bit64::as.integer64( (1:nrow(tbl) + id_max))
  id_max <- max(tbl$id)

  cat("Writing",tsvFile,"to",dbTable,"table in database",dbname,"...\n")
  dbWriteTable(conn = con, name = dbTable, value = tbl, append = TRUE)
  
}

cat("Generating index posIndex...\n")
dbExecute(con, 'CREATE INDEX "posIndex" ON "public"."genes_genes" ("asm", "ann", "position");')


cat("Closing database connection...\n")
dbDisconnect(con)

