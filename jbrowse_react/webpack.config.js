const path = require('path');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  mode: 'production',
  target: 'web',
  entry: {
    index: './src/index.js',
    //jbrowse: './src/jbrowseApp.js'
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  externals : {
    fs: "commonjs fs",
    canvas: {} 
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        loader: ['css-loader']
      },
      {
        test: /\.(jpe?g|png|gif|ico|woff|woff2)$/,
        use: {
          loader: 'url-loader',
        },
      },
    ]
  },
  plugins: [
    //new BundleAnalyzerPlugin()
    //new CompressionPlugin(),
  ]
};