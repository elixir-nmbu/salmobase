
# Install modules:
npm install
npm audit fix

# Create dist/main.js:
npx webpack

# Copy js to django project:
cp dist/main.js /workspace/salmobase_v3/genomes/static/genomes/js/jbrowse/jbrowse_linear_view.js