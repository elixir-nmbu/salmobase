import React from "react";
import ReactDOM from "react-dom";
import Jbrowse from "./jbrowseApp.js"
import "@fontsource/roboto";

const data = JSON.parse(document.getElementById('jbrowse_data').textContent);

ReactDOM.render(<Jbrowse data={data}/>, document.getElementById('jbrowse'));