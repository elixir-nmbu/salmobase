import React from 'react'
import {
  createViewState,
  createJBrowseTheme,
  JBrowseLinearGenomeView,
  ThemeProvider,
} from '@jbrowse/react-linear-genome-view'



const theme = createJBrowseTheme()

function Jbrowse(props) {
  const state = createViewState({
    assembly: props.data.assembly,
    tracks: props.data.tracks,
    location: props.data.location,
    defaultSession: props.data.defaultSession,
  })
  return (
    <ThemeProvider theme={theme}>
      <JBrowseLinearGenomeView viewState={state} />
    </ThemeProvider>
  )
}
export default Jbrowse;
