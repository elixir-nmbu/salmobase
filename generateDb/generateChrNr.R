# Because of inconsistency in chromosome naming between assemblies,
# especially in the older genomes, this script generates more aliases.
#
# The most consistant way of naming chromosome is to use the chromosome number.
# This is what they do in the new Ensembl genomes too, so they can be used as is.
#
# For NCBI genomes it is usually possible to use the `Sequence-Name` after removing
# any prefix.
# 

library(tidyverse)

chrFiles <- 
  dir("../datafiles/TSV/chromosomes",recursive = T,full.names = T) %>% 
  set_names(.,paste0(basename(dirname(dirname(.))),"/",basename(dirname(.))))


tbl <- lapply(chrFiles, function(x){filter(read_tsv(x,col_types = "ccccccld"),is_chromosome)})
  

# note: some chromosomes are split in several contigs/arms.. e.g. 4p, 4q.1:29 and 4q.2
tbl$`DollyVarden/ASM291031v2` %>% 
  mutate(name=sub("AC0?","",`Sequence-Name`)) %>% 
  select(seqname,name) %>% 
  write_tsv("chromosome_numbers/DollyVarden_ASM291031v2.tsv")


tbl$`AtlanticSalmon/ICSASG_v2` %>% 
  mutate(name=sub("ssa0?","",`Sequence-Name`)) %>%
  select(seqname,name) %>% 
  write_tsv("chromosome_numbers/AtlanticSalmon_ICSASG_v2.tsv")


# tbl$`AtlanticSalmon/Ssal_v3.1` %>% 
#   mutate(name = seqname) 
# 
# tbl$`BrownTrout/fSalTru1.1` %>% 
#   mutate(name = seqname) 

tbl$`CohoSalmon/Okis_V1` %>% 
  mutate(name=sub("Okis0?","",`Sequence-Name`)) %>% 
  select(seqname,name) %>% 
  write_tsv("chromosome_numbers/CohoSalmon_Okis_V1.tsv")

tbl$`CohoSalmon/Okis_V2` %>% 
  mutate(name=sub("OkisLG0?","",`Sequence-Name`)) %>% 
  select(seqname,name) %>% 
  write_tsv("chromosome_numbers/CohoSalmon_Okis_V2.tsv")

# tbl$`RainbowTrout/Omyk_1.0` %>% 
#   mutate(name = seqname) %>% View()
# 
# tbl$`RainbowTrout/USDA_OmykA_1.1` %>% 
#   mutate(name = seqname) %>% View()
