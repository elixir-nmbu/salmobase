#!/bin/bash
#SBATCH --ntasks=1               
#SBATCH --nodes=1               
#SBATCH --job-name=import_genomes
#SBATCH --output=slurm-snakemake-%j.out
#SBATCH --partition=smallmem,orion,hugemem,gpu
#SBATCH --no-requeue # possibly avoid restarting if the node fails

# bash strict mode, i.e. stop script on first error
set -euo pipefail

# need conda
module load Anaconda3

# need snakemake
module load snakemake

# need R
module load R/4.0.4


# Set umask so that new files get group write access
umask 002

# Note: need to set up a slurm profile (see snakemake documentation)

snakemake \
  --profile slurm -j 12 --default-resources nodes=1 \
  --cluster-config l_orion_config.json \
  --jobname {rulename}.snakejob.{jobid}.sh \
  --printshellcmds \
  --use-conda \
  --conda-prefix ~/.conda/envs \
  --keep-going \
  $@

# Create conda env:
# snakemake -j1 --use-conda --conda-prefix ~/.conda/envs --conda-create-envs-only