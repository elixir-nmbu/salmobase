
def make_NCBI_url_prefix(acc, asm):
  return( "https://ftp.ncbi.nlm.nih.gov/genomes/all/"+acc[0:3]+"/"+acc[4:7]+"/"+acc[7:10]+"/"+acc[10:13]+"/"+
          acc+"_"+asm+"/"+acc+"_"+asm)
  # E.g.: ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2

