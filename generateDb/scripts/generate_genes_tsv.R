library(tidyverse) 


args = commandArgs(trailingOnly=TRUE)
if ( !(length(args) %in% 3:4)) {
  stop("Requires three or four commandline arguments: <input gff> <src> <output file> [OGtbl_path]", call.=FALSE)
} 
gff <- args[1]
gff_src <- args[2]
outFile <- args[3]
if(length(args)==4){
  hasOG=TRUE
  OGtblFile <- args[4]
} else {
  hasOG=FALSE
}

# ## test Ensembl
# gff <- "../downloads/https__ftp.ensembl.org/pub/release-104/gff3/salmo_salar/Salmo_salar.ICSASG_v2.104.gff3.gz"
# gff_src <- "Ensembl"
# outFile <- "../datafiles/TSVtest/genes/AtlanticSalmon/ICSASG_v2/Ensembl_genes.tsv"
# dir.create(dirname(outFile),recursive = T)
# hasOG=FALSE
# 
# ## test EnsemblRapid
# gff <- "../downloads/ftp__ftp.ensembl.org/pub/rapid-release/species/Salmo_salar/GCA_905237065.2/geneset/2021_07/Salmo_salar-GCA_905237065.2-2021_07-genes.gff3.gz"
# gff_src <- "EnsemblRapid"
# outFile <- "../datafiles/TSVtest/genes/AtlanticSalmon/Ssal_v3.1/EnsemblRapid_genes.tsv"
# dir.create(dirname(outFile),recursive = T)
# hasOG=TRUE
# OGtblFile <- "/mnt/SCRATCH/lagr/ortho_pipeline_2021-11/Ortho_pipeline/OGtbl.tsv"
# 
# 
# ## test NCBI
# gff <- "../downloads/https__ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_genomic.gff.gz"
# gff_src <- "NCBI"
# outFile <- "../datafiles/TSVtest/genes/AtlanticSalmon/ICSASG_v2/NCBI_genes.tsv"
# dir.create(dirname(outFile),recursive = T)
# hasOG=TRUE
# OGtblFile <- "/mnt/SCRATCH/thut/pipeline_results_all_data/Ortho_pipeline/OGtbl_whole_OG.tsv"



# handle cases without OG
if(hasOG){
  OGtbl <- read_tsv(OGtblFile, col_types=cols())
  OGtblSimple <- OGtbl %>% transmute(og = OG, gene_id = as.character(geneID))
}

gff_expandAttr <- function(gffTbl){
  gffTbl %>% 
    # add temporary index
    mutate(idx = 1:n()) %>% 
    # split the attributes
    mutate(attribute = strsplit(attribute,split = ";")) %>% 
    unnest(attribute) %>% 
    separate(attribute,c("key","value"), sep="=") %>% 
    spread(key = key,value = value) %>% 
    select(-idx)
}


addTxProductDesc <- function(tbl,gffTbl){
  # NCBI stores the gene product description in the mRNA feature
  productTbl <-
    gffTbl %>%
    filter(feature == "mRNA") %>% 
    gff_expandAttr() %>% 
    select(Parent, product) %>% 
    # keep only the first if multiple isoforms
    group_by(Parent) %>% slice(1) %>% ungroup() %>%
    # remove the "transcript variant" text.
    mutate(product=sub("%2C transcript variant X[0-9]+$","",product))
  
  tbl %>% 
    # add product of the mRNA
    left_join(productTbl, by=c("ID"="Parent")) %>% 
    # add "[product]" to the product description
    mutate( product = ifelse(!is.na(product), paste(product,"[product]"),NA)) %>% 
    # replace missing gene description with product
    mutate( description = ifelse(is.na(description), product, description)) %>% 
    select( -product )
}

geneTableFromGFF <- function(gff){
  cat("Reading gff file:",gff,"\n")
  gffTbl <- 
    read_tsv(gff,col_names = c("seqname","source","feature","start","end","score","strand","frame","attribute"),
             comment="#", col_types="ccciicccc")
  
  
  #### genes_products table ####
  
  cat("Generating genes table...\n")
  
  # What about other gene types?
  # EnsemblRapid has "gene", "ncRNA_gene", "pseudogene", which all have gene_id's
  # TODO: check with other gffs
  gene_gff <-
    gffTbl %>% 
    filter(feature %in% c("gene", "ncRNA_gene", "pseudogene")) %>% 
    gff_expandAttr()
  
  if(!("version" %in% colnames(gene_gff))){
    if(gff_src %in% c("Ensembl","EnsemblRapid")) 
      stop("No version attribute in Ensembl gene features...")
    gene_gff$version <- NA
  }
  
  if(!("description" %in% colnames(gene_gff))){
    warning("No description attribute in gene features...",immediate. = T)
    gene_gff$description <- NA
  }
  
  if(!("Name" %in% colnames(gene_gff))){
    warning("No Name attribute in gene features...",immediate. = T)
    gene_gff$Name <- NA
  }
  
  
  
  geneTbl <-
    gene_gff %>% 
    
    # position is the 0 based gene index along the chromosome/scaffold
    group_by(seqname) %>% 
    arrange(start) %>% 
    mutate( position = 0:(n()-1)) %>% 
    ungroup() %>%

    # # For NCBI refseq annotations the gene_id is Dbxref GeneID
    { if (gff_src=="NCBI") mutate(., gene_id = sub(".*GeneID:([0-9]+).*","\\1",Dbxref)) else . } %>% 

    # Join with orthogroup id (optionally)
    { if (hasOG) left_join(., OGtblSimple, by="gene_id") else mutate(., og=NA_character_)} %>% 
    
    # # For Ensembl genes we add the version number to the gene_id
    # { if (gff_src %in% c("Ensembl","EnsemblRapid")) mutate(., gene_id = paste(gene_id, version, sep = ".")) else . } %>% 

    # get gene description from product (for NCBI gff)
    { if (gff_src=="NCBI") addTxProductDesc(., gffTbl) else . } %>% 
    
    # rename gene_biotype to biotype (for NCBI gff)
    { if (gff_src=="NCBI") rename(., biotype=gene_biotype) else . } %>% 
    
    # select columns we will keep
    select(seqname, start, end, strand, gene_id, version, gene_name=Name,
           biotype, description, og, position) %>%
    # replace "%2C" with "," and "%3B" with ";"
    mutate(across( c("description"), ~ gsub("%2C",",", .x))) %>% 
    mutate(across( c("description"), ~ gsub("%3B",";", .x)))
  
    

  return(geneTbl)
} 


  
geneTbl <- geneTableFromGFF(gff)
cat("Writing to file:",outFile,"\n")
write_tsv(geneTbl, file = outFile)
cat("Done.\n")


