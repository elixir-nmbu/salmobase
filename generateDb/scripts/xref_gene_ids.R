# get Ensembl-NCBI geneID mappings from Biomart
#

library(dplyr)
library(biomaRt)


args = commandArgs(trailingOnly=TRUE)
if ( !(length(args) == 4)) {
  stop("Requires commandline arguments: <Ensembl_version> <dataset (e.g. “ssalar_gene_ensembl”)> <outEns2NCBI> <outNCBI2Ens>", call.=FALSE)
} 
EnsVer <- args[1]
dataset <- args[2]
outEns2NCBI <- args[3]
outNCBI2Ens <- args[4]

cat("Get biomart URL for version",EnsVer,": ")
Ens_URL <- filter(listEnsemblArchives(), version==EnsVer)$url
cat(Ens_URL,"\n")

cat("Fetching from Ensembl dataset:",dataset,"\n")
tbl <- getBM(attributes = c('ensembl_gene_id', 'entrezgene_id'),useCache = F,
             mart = useDataset(dataset, mart = useMart("ENSEMBL_MART_ENSEMBL",host = Ens_URL)))


cat("Writing:",outEns2NCBI,"\n")
tbl %>% 
  transmute(gene_id = ensembl_gene_id, gene_id_other = entrezgene_id) %>% 
  na.omit() %>%
  write.table(file = outEns2NCBI,sep = "\t",row.names = F,quote = F)

cat("Writing:",outNCBI2Ens,"\n")
tbl %>% 
  transmute(gene_id = entrezgene_id, gene_id_other = ensembl_gene_id) %>%
  na.omit() %>% 
  write.table(file = outNCBI2Ens,sep = "\t",row.names = F,quote = F)

cat("Done.\n")
