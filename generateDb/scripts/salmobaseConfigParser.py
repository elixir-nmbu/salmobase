import yaml
import numpy as np


def make_NCBI_url_prefix(acc, asm):
  return( "https://ftp.ncbi.nlm.nih.gov/genomes/all/"+acc[0:3]+"/"+acc[4:7]+"/"+acc[7:10]+"/"+acc[10:13]+"/"+
          acc+"_"+asm+"/"+acc+"_"+asm)
  # E.g.: ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2

class SalmobaseConfigError(Exception):
  pass

class SalmobaseConfig:
  
  def listAssemblySequences(self):
    retList = []
    for spc in self.species.values():
      for asm in spc.assemblies.values():
        retList.append({'spc': spc.spc, 'asm': asm.asm, **asm.sequence})
    return retList
  
  def listAnnotations(self):
    retList = []
    for spc in self.species.values():
      for asm in spc.assemblies.values():
        for ann in asm.annotations.values(): # What if annotation is missing?
          retList.append({'spc': spc.spc, 'asm': asm.asm, 'ann': ann.ann, **ann.meta})
    return retList

  def listSpeciesOrthology(self):
    retList = []
    for o in self.orthology.values():
      retList.append({'ortho': o.ortho, 'spc': o.spc, 'asm': o.asm, 'ann': o.ann, 'name': o.name})
    return retList   
    
  def getOrthology(self, spc, asm, ann):
    '''
    Get the orthology dict for the given asm/ann
    Note: if an asm/ann is in multiple orthology datasets it will return the first.
    '''
    for orthoDict in self.configDict['orthology'].values():
      if( spc in orthoDict['species']):
        if( orthoDict['species'][spc]['asm']==asm and orthoDict['species'][spc]['ann']==ann):
          return(orthoDict)
    return(None)

  def __init__(self, salmobaseConfigFile):
    
    print(f'Parsing salmobase config ({salmobaseConfigFile})...')
    
    with open(salmobaseConfigFile,'r') as f:
      self.configDict = yaml.safe_load(f)
    
    if not 'species' in self.configDict:
      raise SalmobaseConfigError("'species:' missing")
    
    self.species = {spc: Species(spc, speciesDict) for spc, speciesDict in self.configDict["species"].items()}
    self.orthology = {ortho+"_"+spc: Orthology(ortho, orthologyDict, spc, spcDict) for ortho, orthologyDict in self.configDict["orthology"].items() for spc,spcDict in orthologyDict['species'].items() }
    
    assemblies = {}
    for spc in self.species.values():
      for asm in spc.assemblies.values():
        assemblies[asm.asm] = asm
    self.assemblies = assemblies

def findkeys(node, kv):
    if isinstance(node, list):
        for i in node:
            for x in findkeys(i, kv):
               yield x
    elif isinstance(node, dict):
        if kv in node:
            yield node[kv]
        for j in node.values():
            for x in findkeys(j, kv):
                yield x

class Species:
  def __init__(self, spc, speciesDict):
    print("- species: " + spc)
    self.spc = spc
    self.name_text = speciesDict['name_text']
    self.name_sci = speciesDict['name_sci']
    self.name_short = speciesDict['name_short']
    self.description = speciesDict.get('description','(NO DESCRIPTION)')
    self.assemblies = {asm: Assembly(spc, asm, assemblyDict) for asm, assemblyDict in speciesDict["assemblies"].items()}
    if len(self.assemblies) > 1:
      self.primary_assembly = speciesDict['primary_assembly']
    else:
      # If there is only one assembly it is implicitly the primary assembly
      self.primary_assembly = list(self.assemblies)[0]

class Assembly:
  def __init__(self, spc, asm, assemblyDict):
    print("- - assembly: " + asm)
    self.spc = spc
    self.asm = asm
    self.sequence = assemblyDict['sequence']
    if 'annotations' in assemblyDict:
      self.annotations = {ann: Annotation(spc, asm, ann, annotationDict) for ann, annotationDict in assemblyDict["annotations"].items()}
      if len(assemblyDict["annotations"]) > 1:
        self.primary_annotation = assemblyDict['primary_annotation']
      else:
        self.primary_annotation = list(self.annotations)[0]
    else:
      self.annotations = {}
  
  def getGenbankLink(self):
    acc = self.sequence['accession']
    return(f'https://www.ncbi.nlm.nih.gov/assembly/{acc}/')
  
  def getFastaLink(self):
    if(self.sequence['source'] == "NCBI"):
      acc = self.sequence['accession']
      return( make_NCBI_url_prefix(acc, self.asm) + '_genomic.fna.gz')
    else:
      return( self.sequence['fasta_url'] )  
  

class Annotation:
  def __init__(self, spc, asm, ann, annotationDict):
    print("- - - annotation: " + ann)
    self.spc = spc
    self.asm = asm
    self.ann = ann
    self.meta = annotationDict
  
  def getGffLink(self):
    if(self.ann == "NCBI"):
      acc = self.meta['accession']
      return( make_NCBI_url_prefix(acc, self.asm) + '_genomic.gff.gz')
    else:
      return( self.meta['gff_url'] )


class Orthology:
  def __init__(self, ortho, orthologyDict, spc, spcDict):
    self.ortho = ortho
    self.prefix = orthologyDict['prefix']
    self.spc = spc
    self.asm = spcDict['asm']
    self.ann = spcDict['ann']
    self.name = spcDict['name']
    print("- orthology: " + ortho + "(" + orthologyDict['prefix'] + ") " + spc + " (asm:"+spcDict['asm']+" ann:"+spcDict['ann']+")")
######################################################



try:
  # load the salmobase definition
  salmobase = SalmobaseConfig('species.yml')
except KeyError as e:
  raise SalmobaseConfigError("Missing: "+str(e))


