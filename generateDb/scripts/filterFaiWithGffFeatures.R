# This script generates an alias file for JBrowse
# The alias file is a tab separated table with the refernce sequence ID in the first column and
# alternative identifiers in the other columns.
# This script takes Ensembl GFF files which includes an "alias" attribute 
# for each region.


library(tidyverse)

cat("filterFaiWithGffFeatures\n")


args = commandArgs(trailingOnly=TRUE)
if (length(args)!=3) {
  stop("Requires two commandline arguments: <input gff> <input fai file> <output filtered fai file>", call.=FALSE)
} 
gff = args[1]
fai = args[2]
outfile_filtered = args[3]

# gff = "../datafiles/genomes/AtlanticSalmon/ICSASG_v2/annotations/Ensembl/Salmo_salar.ICSASG_v2.104_filtered.gff.gz"
# fai = "../datafiles/genomes/AtlanticSalmon/ICSASG_v2/sequence_Ensembl/Salmo_salar.ICSASG_v2.dna_sm.toplevel.fa.gz.fai"
# outfile_filtered = "../datafiles/genomes/AtlanticSalmon/ICSASG_v2/sequence_Ensembl/Salmo_salar.ICSASG_v2.dna_sm.toplevel_filtered.fa.gz.fai"

cat("Reading GFF file:",gff,"...\n")

gffTbl <- 
  read_tsv(gff,col_names = c("seqname"),comment="#", col_types="c--------")
  
# identify sequences with features (ignoring "biological_region" feature)
seq_withFeatures <-
  gffTbl %>%
  with(unique(seqname))

l <- read_lines(fai)
seq <- sapply(strsplit(l,split = "\\t"),"[",1)
l_out <- l[seq %in% seq_withFeatures]
write_lines(l_out,file = outfile_filtered)
