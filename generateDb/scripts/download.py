from re import sub


# Downloaded files are stored in a path that corresponds to the URL
# e.g.: "https://example.com/file.txt" is stored as "downloads/https__example.com/file.txt"
# To get a valid path the "://" in the url is replaced with "__"

def download_url_to_path(url):
  return sub('(http|https|ftp)://(.*)',r'\1__\2',url)

def download_path_to_url(urlpath):
  return sub('(http|https|ftp)__(.*)',r'\1://\2',urlpath)
  
