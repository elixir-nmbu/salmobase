import subprocess

# This script creates the jbrowse config using the species.yml info for assemblies and annotations.
# Jbrowse config uses url links to salmobase.org datafiles.

# Path to jbrowse to put config
jb_dir = "/datafiles/jbrowse"

cmd = f'jbrowse create {jb_dir} --force'
process = subprocess.run(cmd.split())

# Could just replace this with a bash script