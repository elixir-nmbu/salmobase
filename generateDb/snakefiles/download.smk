from scripts.download import download_path_to_url

rule downloadFiles:
  output: download_dir + "/{urlpath}"
  params: URL = lambda w: download_path_to_url(w.urlpath)
  shell: "wget --no-verbose --retry-connrefused --tries=3 --no-check-certificate {params.URL} -O {output}"
