import os
from scripts.salmobaseConfigParser import salmobase
from scripts.NCBI_common import make_NCBI_url_prefix
from scripts.download import download_url_to_path

# This script downloads annotations (i.e. gff) from NCBI/Ensembl and 
# generates the filtered and tabix indexed files for JBrowse

# config: 
# - species
# - - {spc}
# - - - assemblies
# - - - - {asm} (assembly name. NB: must match that used in NCBI URL)
# - - - - - annotations
# - - - - - - {ann} (either NCBI or Ensembl)
# - - - - - - - release =  (Ensembl release or NCBI annotation release)
# - - - - - - - accession = {acc} (for NCBI, refseq or genbank assembly accession, e.g. "GCF_000233375.1")
# - - - - - - - gff_url = (for Ensembl, url to the gff file)
#
# output: 
#   data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff.gz"
#   data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff.gz.tbi"
#
# The {filenamebase} should match the downloaded filename


# Workflow overview:
# 1. download files (construct URLs)
# 2. unzip + filter (remove region and cDNA_match features)
# 3. sort
# 4. bgzip
# 5. tabix



# listAssemblySequences() returns a list with:
# {'spc': {spc}, 'asm': {asm}, 'ann': {ann}, 'release', 'accession': {acc}}

##
# NCBI

annotationsNCBI = [x for x in salmobase.listAnnotations() if x['ann'] == 'NCBI']

all_out += [multiext(data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/annotations/NCBI/{x["accession"]}_{x["asm"]}_genomic_filtered.gff','.gz','.gz.tbi') for x in annotationsNCBI]


##
# Ensembl/EnsemblRapid

annotationsEnsembl = [x for x in salmobase.listAnnotations() if x['ann'] == 'Ensembl']
annotationsEnsemblRapid = [x for x in salmobase.listAnnotations() if x['ann'] == 'EnsemblRapid']


def gff_url_to_basename(url):
  return( os.path.basename(url).replace('.gff3.gz','') )

all_out += [multiext(data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/annotations/Ensembl/{gff_url_to_basename(x["gff_url"])}_filtered.gff','.gz','.gz.tbi') for x in annotationsEnsembl]
all_out += [multiext(data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/annotations/EnsemblRapid/{gff_url_to_basename(x["gff_url"])}_filtered.gff','.gz','.gz.tbi') for x in annotationsEnsemblRapid]

def get_ensembl_gff_url(w):
  return salmobase.species[w.spc].assemblies[w.asm].annotations["Ensembl"].meta['gff_url']

rule filterGFF_ensembl:
  input: lambda w: download_dir + "/" + download_url_to_path( get_ensembl_gff_url(w) )
  output: temp(data_dir + "/genomes/{spc}/{asm}/annotations/Ensembl/{filenamebase}_filtered_unsorted.gff")
  shell: "gunzip -c {input} | awk '{{ if ($3 != \"region\" && $3 != \"biological_region\" && $3 != \"cDNA_match\") {{ print }} }}' > {output}"

def get_ensemblRapid_gff_url(w):
  return salmobase.species[w.spc].assemblies[w.asm].annotations["EnsemblRapid"].meta['gff_url']


rule filterGFF_ensemblRapid:
  input: lambda w: download_dir + "/" + download_url_to_path( get_ensemblRapid_gff_url(w) )
  output: temp(data_dir + "/genomes/{spc}/{asm}/annotations/EnsemblRapid/{filenamebase}_filtered_unsorted.gff")
  shell: "gunzip -c {input} | awk '{{ if ($3 != \"region\" && $3 != \"biological_region\" && $3 != \"cDNA_match\") {{ print }} }}' > {output}"


rule filterGFF_NCBI:
  input: lambda w: download_dir + "/" + download_url_to_path( make_NCBI_url_prefix(w.acc, w.asm) +"_genomic.gff.gz" )
  output: temp(data_dir + "/genomes/{spc}/{asm}/annotations/NCBI/{acc}_{asm}_genomic_filtered_unsorted.gff")
  shell: "gunzip -c {input} | awk '{{ if ($3 != \"region\" && $3 != \"cDNA_match\") {{ print }} }}' > {output}"

rule sortGFF:
  input:       data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered_unsorted.gff"
  output: temp(data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff")
  conda: "../env.yml"
  shell: "gt gff3 -sortlines -tidy -retainids {input} > {output}"

rule bgzipGFF:
  input:  data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff"
  output: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff.gz"
  conda: "../env.yml"
  shell: "bgzip {input}"

rule tabixGFF:
  input: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff.gz"
  output: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff.gz.tbi"
  conda: "../env.yml"
  shell: "tabix {input}"
