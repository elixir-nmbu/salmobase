import os
from scripts.salmobaseConfigParser import salmobase
from scripts.download import download_url_to_path

# Generate gene tables for all annotations. To be imported into the database.

# get list of annotations from species.yml
annotations = salmobase.listAnnotations()

# output files are:
# data_dir + "/TSV/genes/{spc}/{asm}/{ann}_genes.tsv
all_out += [data_dir + f'/TSV/genes/{x["spc"]}/{x["asm"]}/{x["ann"]}_genes.tsv' for x in annotations]


def getGFFfile(w):
  gff_url = salmobase.species[w.spc].assemblies[w.asm].annotations[w.ann].getGffLink()
  return( download_dir + '/' + download_url_to_path(gff_url) )

def getOGtbl(w):
  orthology = salmobase.getOrthology(w.spc,w.asm,w.ann)
  if( orthology == None ):
    return([])
  return(data_dir + '/' + orthology["datafiles"]["OGtbl"])

rule generate_genes_tsv:
  input:
    gff = getGFFfile,
    OGtbl = getOGtbl
  output: data_dir + "/TSV/genes/{spc}/{asm}/{ann}_genes.tsv"
  shell: "Rscript scripts/generate_genes_tsv.R {input.gff} {wildcards.ann} {output} {input.OGtbl}"
