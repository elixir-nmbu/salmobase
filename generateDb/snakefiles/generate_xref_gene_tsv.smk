from scripts.salmobaseConfigParser import salmobase

# Get all assemblies with both NCBI and Ensembl annotations
asmWithBothAnn = []
asm2ensver = {} # Ensembl release for each asm
for spc in salmobase.species.values():
  for asm in spc.assemblies.values():
    if( ('Ensembl' in asm.annotations) and ('NCBI' in asm.annotations)):
      asmWithBothAnn.append({'spc': spc.spc, 'asm': asm.asm })
      asm2ensver[asm.asm] = asm.annotations['Ensembl'].meta['release']



# output files are:
# data_dir + "/TSV/xref_gene_ids/{spc}/{asm}/{ann}_to_{otherAnn}.tsv
all_out += [data_dir + f'/TSV/xref_gene_ids/{x["spc"]}/{x["asm"]}/Ensembl_to_NCBI.tsv' for x in asmWithBothAnn]
all_out += [data_dir + f'/TSV/xref_gene_ids/{x["spc"]}/{x["asm"]}/NCBI_to_Ensembl.tsv' for x in asmWithBothAnn]

def get_dataset(w):
  """ get ensembl dataset for a species, e.g. "Salmo salar" = "ssalar_gene_ensembl" """
  words = salmobase.species[w.spc].name_sci.split()
  return(words[0][0].lower() + words[1] + "_gene_ensembl")

rule generate_xref_gene_tsv:
  output:
    ens2ncbi = data_dir + "/TSV/xref_gene_ids/{spc}/{asm}/Ensembl_to_NCBI.tsv",
    ncbi2ens = data_dir + "/TSV/xref_gene_ids/{spc}/{asm}/NCBI_to_Ensembl.tsv"
  params:
    ensver = lambda w: asm2ensver[w.asm],
    dataset = get_dataset
  shell:
    "Rscript scripts/xref_gene_ids.R {params.ensver} {params.dataset} {output.ens2ncbi} {output.ncbi2ens}"
