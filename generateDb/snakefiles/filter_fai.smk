import os
from scripts.salmobaseConfigParser import salmobase

# Some genomes have very many unplaced scaffolds,which results in a large fai (fasta index)
# and slow loading time of JBrowse. This script removes sequences without any features from
# the fai file.

#   data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz.filtered.fai"


# listAssemblySequences() returns a list with:
# {'spc': {spc}, 'asm': {asm}, 'source': 'NCBI', 'accession': {acc}}
assemblyToFilter = [x for x in salmobase.listAssemblySequences() if 'filter_fai' in x and x['filter_fai'] ]


def get_basename(x):
  '''
  Get filenamebase of fasta given the assembly config x
  '''
  if( x['source'] == "NCBI"):
    return( f'{x["accession"]}_{x["asm"]}_genomic')
  return( os.path.basename(x["fasta_url"]).replace('.fa.gz','') )


all_out += [data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/sequence_{x["source"]}/{get_basename(x)}.fa.gz.filtered.fai' for x in assemblyToFilter]


rule filter_fai:
  input:    # note: the annotation name needs to match the sequence source
    gff=data_dir + "/genomes/{spc}/{asm}/annotations/{src}/{filenamebase}_filtered.gff.gz",
    fai=data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz.fai",
  output: data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz.filtered.fai"
  shell: "Rscript scripts/filterFaiWithGffFeatures.R {input.gff} {input.fai} {output}"
