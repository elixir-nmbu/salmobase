from scripts.salmobaseConfigParser import salmobase

# Generate OG tables and gene-tree tables. To be imported into the database.

# get list of orthogroup sets from species.yml
orthology = salmobase.configDict["orthology"]

# output files are:
all_out += [data_dir + f'/TSV/og/{OGdataset}.tsv' for OGdataset in orthology]
all_out += [data_dir + f'/TSV/genetrees/{OGdataset}_allTrees_nhx.tsv' for OGdataset in orthology]

rule generate_og_tsv:
  input: lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['OGtbl']
  output: data_dir + "/TSV/og/{OGdataset}.tsv"
  params: 
    N_teleost = lambda w: orthology[w.OGdataset]['OG_nodes']['teleost'],
    N_salmonid = lambda w: orthology[w.OGdataset]['OG_nodes']['salmonid']
  shell: "Rscript scripts/generate_og_tsv.R {input} {output} {params.N_teleost} {params.N_salmonid}"
  
rule copy_trees_tsv:
  input: lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['geneTrees']
  output: data_dir + "/TSV/genetrees/{OGdataset}_allTrees_nhx.tsv"
  shell: "cp {input} {output}"
