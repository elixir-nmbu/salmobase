from scripts.salmobaseConfigParser import salmobase
from itertools import combinations

# Generate OG tables and gene-tree tables. To be imported into the database.

# get list of orthogroup sets from species.yml
orthology = salmobase.configDict["orthology"]

# output files are:

for OGdataset in orthology:
  spcs = [species['name'] for species in orthology[OGdataset]['species'].values()]
  all_out += [data_dir + f'/comparative/orthologs/{OGdataset}/paralogs_{x}_{x}.paf.gz' for x in spcs]
  spcs_pairs = list(combinations(spcs, 2))
  all_out += [data_dir + f'/comparative/orthologs/{OGdataset}/orthologs_{x[0]}_{x[1]}.paf.gz' for x in spcs_pairs]

rule generate_ortholog_paf:
  input:
    OGtbl_path = lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['OGtbl'],
    genePosTbls_dir = lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['genePosTblsDir']
  output: data_dir + '/comparative/orthologs/{OGdataset}/{parOrOrtho}logs_{spc_1}_{spc_2}.paf.gz'
  params: 
    N_teleost = lambda w: orthology[w.OGdataset]['OG_nodes']['teleost']
  shell:
    """
    # <OGtbl_path> <genePosTbls_dir> <teleost_node> <spc_1> <spc_2> <output paf file>  
    Rscript scripts/generate_ortholog_paf.R {input.OGtbl_path} {input.genePosTbls_dir} \
      {params.N_teleost} {wildcards.spc_1} {wildcards.spc_2} {output}
    """


