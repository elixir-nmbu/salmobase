from scripts.salmobaseConfigParser import salmobase

# Generate synteny tables. To be imported into the database.

# get list of orthogroup sets from species.yml
orthology = salmobase.configDict["orthology"]
species_orthology = salmobase.listSpeciesOrthology()

# output files are:
generate_synteny_out  = [data_dir + f'/TSV/synteny/{x["ortho"]}/{x["spc"]}/synteny.tsv' for x in species_orthology]
generate_synteny_out += [data_dir + f'/TSV/synteny/{x["ortho"]}/{x["spc"]}/band.tsv' for x in species_orthology]
all_out += generate_synteny_out

rule generate_synteny_all:
  input: generate_synteny_out


def getChromosomeTSVforOrthology(w):
  asm  = orthology[w.OGdataset]['species'][w.spc]['asm']
  return data_dir + "/TSV/chromosomes/{spc}/"+asm+"/chromosomes.tsv"

rule generate_synteny_tsv:
  input: 
    speciesTree = lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['speciesTree'],
    iadhoreResDir = lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['iadhoreResDir'],
    genePosTblsDir = lambda w: data_dir + "/" + orthology[w.OGdataset]['datafiles']['genePosTblsDir'],
    chromosome_tsv = getChromosomeTSVforOrthology
  output:
    synteny = data_dir + "/TSV/synteny/{OGdataset}/{spc}/synteny.tsv",
    band = data_dir + "/TSV/synteny/{OGdataset}/{spc}/band.tsv"
  params:
    short_name = lambda w: orthology[w.OGdataset]['species'][w.spc]['name'],
    ann = lambda w: orthology[w.OGdataset]['species'][w.spc]['ann'],
    N_salmonid = lambda w: orthology[w.OGdataset]['OG_nodes']['salmonid_all']
  shell:
    """
    Rscript scripts/generate_synteny_tsv.R {output.synteny} {output.band}\
      {input.chromosome_tsv} {input.iadhoreResDir} {input.genePosTblsDir} {input.speciesTree}\
      {params.short_name} {params.ann} {params.N_salmonid}
    """
  

