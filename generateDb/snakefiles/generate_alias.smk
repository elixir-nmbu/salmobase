from scripts.salmobaseConfigParser import salmobase
from scripts.download import download_url_to_path
from scripts.NCBI_common import make_NCBI_url_prefix


# Identify assemblies with assembly report on NCBI
asm_with_NCBI_asmReport = []
asmReport_acc = {}
for spc in salmobase.species.values():
  for asm in spc.assemblies.values():
    if('NCBI' in asm.annotations):
      asm_with_NCBI_asmReport.append({'spc': spc.spc, 'asm': asm.asm})
      asmReport_acc[asm.asm] = asm.annotations['NCBI'].meta['accession']
    elif( asm.sequence['accession'].startswith('GCA_')):
      asm_with_NCBI_asmReport.append({'spc': spc.spc, 'asm': asm.asm})
      asmReport_acc[asm.asm] = asm.sequence['accession']
      

generate_alias_out = [data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/alias.txt' for x in asm_with_NCBI_asmReport]
generate_alias_out += [data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/alias_filtered.txt' for x in asm_with_NCBI_asmReport]
generate_alias_out += [data_dir + f'/TSV/chromosomes/{x["spc"]}/{x["asm"]}/chromosomes.tsv' for x in asm_with_NCBI_asmReport]

all_out += generate_alias_out

rule generate_alias_all:
  input: generate_alias_out


def get_gff_ens_path(w):
  # get url of ensembl annotation
  asm = salmobase.species[w.spc].assemblies[w.asm]
  if('Ensembl' in asm.annotations):
    ann = asm.annotations['Ensembl']
  elif('EnsemblRapid' in asm.annotations):
    ann = asm.annotations['EnsemblRapid']
  else: 
    return []
  
  return download_dir + "/" + download_url_to_path( ann.meta['gff_url'] )

def get_asm_report_url(w):
  return make_NCBI_url_prefix(asmReport_acc[w.asm],w.asm) + "_assembly_report.txt"

def getWhichRef(w):
  ref = salmobase.species[w.spc].assemblies[w.asm].sequence['source']
  if ref == 'EnsemblRapid': # only NCBI/Ensembl is valid
    ref = 'Ensembl'
  return ref

def get_chromosome_numbers_tsv(w):
  if( 'chromosome_numbers_tsv' in salmobase.species[w.spc].assemblies[w.asm].sequence):
    return salmobase.species[w.spc].assemblies[w.asm].sequence['chromosome_numbers_tsv']
  else:
    return 'NA'
  

rule aliasFromMultipleMultipleSources:
  input:
    gff_ens = lambda w: get_gff_ens_path(w),
    asm_report_path = lambda w: download_dir + "/" + download_url_to_path( get_asm_report_url(w) )
  output:
    alias = data_dir + '/genomes/{spc}/{asm}/alias.txt',
    alias_filtered = data_dir + '/genomes/{spc}/{asm}/alias_filtered.txt',
    chromosomes_tsv = data_dir + "/TSV/chromosomes/{spc}/{asm}/chromosomes.tsv"  
  params:
    whichRef = getWhichRef,
    chromosome_numbers_tsv = get_chromosome_numbers_tsv
  shell:
    """
    Rscript scripts/aliasFromMultipleSources.R {output.alias} {output.alias_filtered} {output.chromosomes_tsv}\
      {params.whichRef} {input.asm_report_path} {params.chromosome_numbers_tsv} {input.gff_ens}
    """
