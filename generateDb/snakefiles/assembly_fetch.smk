import os
from scripts.salmobaseConfigParser import salmobase
from scripts.NCBI_common import make_NCBI_url_prefix
from scripts.download import download_url_to_path

# This script downloads assemblies (i.e. genome sequence) from NCBI/Ensembl and 
# generates the indexed assembly files for JBrowse

# config: 
# - species
# - - {spc}
# - - - assemblies
# - - - - {asm} (assembly name. NB: must match that used in NCBI URL)
# - - - - - sequence
# - - - - - - source = {src} ('NCBI' or 'Ensembl' or 'EnsemblRapid')
# - - - - - - fasta_url = (only Ensembl. URL to download fasta file)
# - - - - - - accession = {acc} (refseq or genbank assembly accession, e.g. "GCF_000233375.1")
#
# output: 
#   data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz" ({filenamebase} should match that of the source file)
#   data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz.gzi"
#   data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz.fai"

# Workflow overview:
# 1. get the parameters for the assembly sequences
# 2. download files (construct URLs)
# 3. unzip
# 4. bgzip
# 5. index


# listAssemblySequences() returns a list with:
# {'spc': {spc}, 'asm': {asm}, 'source': 'NCBI', 'accession': {acc}}
assemblySequencesNCBI = [x for x in salmobase.listAssemblySequences() if x['source'] == 'NCBI']

all_out += [multiext(data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/sequence_NCBI/{x["accession"]}_{x["asm"]}_genomic','.fa.gz','.fa.gz.gzi','.fa.gz.fai') for x in assemblySequencesNCBI]

# Ensembl sequences

assemblySequencesEnsembl = [x for x in salmobase.listAssemblySequences() if x['source'] in ['Ensembl','EnsemblRapid']]

def fasta_url_to_basename(url):
  return( os.path.basename(url).replace('.fa.gz','') )

all_out += [multiext(data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/sequence_{x["source"]}/{fasta_url_to_basename(x["fasta_url"])}','.fa.gz','.fa.gz.gzi','.fa.gz.fai') for x in assemblySequencesEnsembl]

rule unzipGenome_NCBI:
  input: lambda w: download_dir + "/" + download_url_to_path(make_NCBI_url_prefix(w.acc, w.asm) +"_genomic.fna.gz")
  output: data_dir + "/genomes/{spc}/{asm}/sequence_NCBI/{acc}_{asm}_genomic.fa"
  shell: "gunzip -c {input} > {output}"

def get_ensembl_fasta_url(w):
  return salmobase.species[w.spc].assemblies[w.asm].sequence['fasta_url']

rule unzipGenome_Ensembl:
  input: lambda w: download_dir + "/" + download_url_to_path( get_ensembl_fasta_url(w) )
  output: data_dir + "/genomes/{spc}/{asm}/sequence_{src,Ensembl|EnsemblRapid}/{filenamebase}.fa"
  shell: "gunzip -c {input} > {output}"


rule bgzipGenome:
  input: data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa"
  output: data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz"
  conda: "../env.yml"
  shell: "bgzip -i {input}"

rule faidxGenome:
  input: data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa.gz"
  output: multiext( data_dir + "/genomes/{spc}/{asm}/sequence_{src}/{filenamebase}.fa", ".gz.fai", ".gz.gzi" )
  conda: "../env.yml"
  shell: "samtools faidx {input}"
