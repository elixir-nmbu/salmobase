import os
from scripts.salmobaseConfigParser import salmobase
from scripts.download import download_url_to_path

# Generate transcript tables for all annotations. To be imported into the database.

# get list of annotations from species.yml
annotations = salmobase.listAnnotations()

# output files are:
# data_dir + "/TSV/genes/{spc}/{asm}/{ann}_genes.tsv
all_out += [data_dir + f'/TSV/transcripts/{x["spc"]}/{x["asm"]}/{x["ann"]}_transcripts.tsv' for x in annotations]


def getGFFfile(w):
  gff_url = salmobase.species[w.spc].assemblies[w.asm].annotations[w.ann].getGffLink()
  return( download_dir + '/' + download_url_to_path(gff_url) )

rule generate_transcripts_tsv:
  input:
    gff = getGFFfile,
  output: data_dir + "/TSV/transcripts/{spc}/{asm}/{ann}_transcripts.tsv"
  shell: "Rscript scripts/generate_transcripts_tsv.R {input.gff} {wildcards.ann} {output}"
