import os
from scripts.salmobaseConfigParser import salmobase
from scripts.NCBI_common import make_NCBI_url_prefix
from scripts.download import download_url_to_path


# generating blastdb
# need gffread and blast+ .. add to env
# 
# For each annotation
#   generate protein and rna db
#   extract protein/rna sequences from gff and fasta
#   unzip gff and fasta
#     if fasta source is not same as annotation (i.e. it is NCBI) then download from NCBI

annotationList = salmobase.listAnnotations()

assemblyList = salmobase.listAssemblySequences()


all_out += [data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/sequence_{x["source"]}/blastdb/dna/{x["spc"]}_{x["asm"]}_dna_blastdb.ndb' for x in assemblyList]
all_out += [data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/annotations/{x["ann"]}/blastdb/rna/{x["spc"]}_{x["asm"]}_{x["ann"]}_rna_blastdb.ndb' for x in annotationList]
all_out += [data_dir + f'/genomes/{x["spc"]}/{x["asm"]}/annotations/{x["ann"]}/blastdb/protein/{x["spc"]}_{x["asm"]}_{x["ann"]}_protein_blastdb.pdb' for x in annotationList]

rule makeblastdb_dna:
  input: scratch_dir + "/genomes/{spc}/{asm}/{ann}_genome_unzip.fa"
  output: data_dir + "/genomes/{spc}/{asm}/sequence_{ann}/blastdb/dna/{spc}_{asm}_dna_blastdb.ndb"
  params: prefix = data_dir + "/genomes/{spc}/{asm}/sequence_{ann}/blastdb/dna/{spc}_{asm}_dna_blastdb"
  conda: "../env.yml"
  shell: "makeblastdb -in {input} -dbtype nucl -input_type fasta -out {params.prefix}"


def get_gff_basename(w):
  url = salmobase.species[w.spc].assemblies[w.asm].annotations[w.ann].getGffLink()
  gff_basename = os.path.basename(url).replace('.gff.gz','').replace('.gff3.gz','')
  return(gff_basename)

rule makeblastdb_rna:
  input: lambda w: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/"+get_gff_basename(w)+"_rna.fna"
  output: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/blastdb/rna/{spc}_{asm}_{ann}_rna_blastdb.ndb"
  params: prefix = data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/blastdb/rna/{spc}_{asm}_{ann}_rna_blastdb"
  conda: "../env.yml"
  shell: "makeblastdb -in {input} -dbtype nucl -input_type fasta -out {params.prefix}"


rule makeblastdb_protein:
  input: lambda w: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/"+get_gff_basename(w)+"_protein.faa"
  output: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/blastdb/protein/{spc}_{asm}_{ann}_protein_blastdb.pdb"
  params: prefix = data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/blastdb/protein/{spc}_{asm}_{ann}_protein_blastdb"
  conda: "../env.yml"
  shell: "makeblastdb -in {input} -dbtype prot -input_type fasta -out {params.prefix}"


rule extract_rna:
  input:
    gff = data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered_unzip.gff",
    fasta = scratch_dir + "/genomes/{spc}/{asm}/{ann}_genome_unzip.fa"
  output: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_rna.fna"
  conda: "../env.yml"
  shell: "gffread -w {output} -g {input.fasta} {input.gff}"


rule extract_protein:
  input:
    gff = data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered_unzip.gff",
    fasta = scratch_dir + "/genomes/{spc}/{asm}/{ann}_genome_unzip.fa"
  output: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_protein.faa"
  conda: "../env.yml"
  shell: "gffread -y {output} -g {input.fasta} {input.gff}"


# if the annotation is not same as the sequence source download the sequence first
def get_genome_fasta_gz_file(w):
  # check if sequence source is same as annotation
  src = salmobase.species[w.spc].assemblies[w.asm].sequence['source']
  if w.ann == src:
    url = salmobase.species[w.spc].assemblies[w.asm].getFastaLink()
    sequence_basename = os.path.basename(url).replace('.fa.gz','').replace('.fna.gz','')
    return(data_dir + f'/genomes/{w.spc}/{w.asm}/sequence_{w.ann}/{sequence_basename}.fa.gz')
  
  # else download it
  if w.ann != 'NCBI': # only supports downloading from NCBI
    raise Exception(f'Cannot get genome fasta for {w.spc}({w.asm}) annotation {w.ann}')
  
  acc = salmobase.species[w.spc].assemblies[w.asm].annotations[w.ann].meta['accession']
  return(download_dir + "/" + download_url_to_path(make_NCBI_url_prefix(acc, w.asm) +"_genomic.fna.gz"))


rule unzip_genome:
  input: get_genome_fasta_gz_file
  output: temp(scratch_dir + "/genomes/{spc}/{asm}/{ann}_genome_unzip.fa")
  shell:
    """
    zcat {input} > {output}
    # Make sure to delete any old .fai in the unlikely situation where the source
    # fasta has been updated.
    rm -f {output}.fai
    """

rule unzip_gff:
  input: data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered.gff.gz"
  output: temp(data_dir + "/genomes/{spc}/{asm}/annotations/{ann}/{filenamebase}_filtered_unzip.gff")
  shell: "zcat {input} > {output}"


