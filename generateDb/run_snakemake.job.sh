#!/bin/bash
#SBATCH --ntasks=5
#SBATCH --nodes=1
#SBATCH --job-name=import_genomes
#SBATCH --output=slurm-snakemake-%j.out
#SBATCH --partition=smallmem,gpu,hugemem
#SBATCH --no-requeue # possibly avoid restarting if the node fails

# bash strict mode, i.e. stop script on first error
set -euo pipefail

# need snakemake
module load snakemake


snakemake \
  --cores $SLURM_NTASKS\
  --use-conda \
  --printshellcmds

# Dry-run:
# snakemake -n -j1 --printshellcmds
#
# Create conda env:
# snakemake -j1 --use-conda --conda-create-envs-only