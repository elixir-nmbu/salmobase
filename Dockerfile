FROM python:3.8.15-bullseye

WORKDIR /app

# install python requirements
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# install apache
RUN apt-get -qq update &&\
    apt-get install -y apache2 apache2-dev

# install jbrowse with nodejs(nvm) under static datafiles
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash &&\
    export NVM_DIR="$HOME/.nvm" &&\
    [ -s "$NVM_DIR/nvm.sh" ] &&\. "$NVM_DIR/nvm.sh" &&\
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" &&\
    nvm install 14 &&\
    npm install -g @jbrowse/cli

# install R (for writing tables into database)    
RUN apt-get install -y --no-install-recommends r-base
RUN R -e "install.packages(c('DBI', 'RPostgres','yaml'))"    

# install blast
RUN apt-get install -y ncbi-blast+

# install logrotate
RUN apt-get install -y logrotate

# setup logrotate config
COPY server_config/logrotate/salmobase /etc/logrotate.d/salmobase
# add cron job (every hour)
COPY server_config/logrotate/logrotate.sh .
RUN echo "0 * * * * /app/logrotate.sh" | crontab -


# apache configuration for salmobase
COPY server_config/apache2/salmobase.conf /etc/apache2/sites-available/salmobase.conf
RUN a2dissite 000-default &&\
    a2ensite salmobase  &&\
    a2enmod headers &&\
    a2enmod proxy_http &&\
    a2enmod proxy

# copy the django app, start script and species.yml
COPY salmobase ./salmobase
COPY start-server.sh .
COPY generateDb/species.yml .

# collect static
ENV STATIC_ROOT="/app/static"
# Note: have to create the /logs dir temporarily as it is not mounted yet
RUN mkdir /logs && python salmobase/manage.py collectstatic

# copy scripts needed to populate database on server
COPY writeDb ./
# copy scripts needed for seting up JBrowse
COPY generateDb/setup_jbrowse.py .
RUN mkdir scripts
COPY generateDb/scripts/salmobaseConfigParser.py scripts

