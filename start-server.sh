#!/bin/bash

echo Starting cron service..
service cron start
echo Starting Apache server...
apachectl start
echo Starting Django server...
python $SALMOBASE_PATH/manage.py runserver
