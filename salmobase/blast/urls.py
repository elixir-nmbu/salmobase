from django.urls import path
from . import views

app_name = 'blast'

urlpatterns = [
    path('input', views.input, name='input'),
    path('result', views.result, name='result'),
]