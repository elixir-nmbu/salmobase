import re
from django.shortcuts import render
from django.http import Http404
from Bio.Blast import NCBIXML
import subprocess 
import tempfile
from salmobase.species import nameToText, getSequenceSource, getAssemblyNames, nameToName, getAnnotations, getPrimaryAssembly, getAllSpecies, isValidSpeciesName
from django.conf import settings
import logging
from genes.models import Transcripts, Genes

# Get an instance of a logger
logger = logging.getLogger("django")





def input(request):
    context = {
        'blastDBlist' : {x.spc: {y.asm: [z for z in y.annotations] for y in x.assemblies.values()} for x in getAllSpecies().values()},
        'primaryAsm' : {spc: getPrimaryAssembly(spc) for spc in getAllSpecies()}
    }

    return render(request, 'blast/input.html', context)

def result(request):
    seq = request.GET.get('seq', '')
    species = request.GET.get('species', '')
    assembly = request.GET.get('assembly', '')
    dbType = request.GET.get('dbType', '')
    blastType = request.GET.get('blastType', '')


    # validate input
    if not blastType in ['blastn', 'blastp', 'blastx', 'tblastn']:
        raise Http404(f'"{blastType}" is not a valid blast searchType')
    if not isValidSpeciesName(species):
        raise Http404(f'"{species}" is not a valid species name')
    if not assembly in getAssemblyNames(species):
        raise Http404(f'"{assembly}" is not a valid assembly for {species}')
    # dbType can be e.g. dna or rna_NCBI, protein_Ensembl etc.
    if not (dbType == 'dna' or re.search("^(rna|protein)_[^_]+$",dbType) != None):
        raise Http404(f'"{dbType}" is not a valid dbType')

    species = nameToName(species) # make sure the case is correct

    # build the blastdb path
    db = f'{settings.DATAFILES_BASE_PATH}/genomes/{species}/{assembly}'
    if(dbType == 'dna'):
        src = getSequenceSource(species,assembly)
        db = db+f'/sequence_{src}/blastdb/dna/{species}_{assembly}_dna_blastdb'
        dbTypeText = 'Whole genome'
    else:
        (molType, annotation) = dbType.split('_')
        if not annotation in list(getAnnotations(species,assembly)):
            raise Http404(f'"{annotation}" is not a valid annotation for {species} ({assembly})')

        db = db+f'/annotations/{annotation}/blastdb/{molType}/' +\
                f'{species}_{assembly}_{annotation}_{molType}_blastdb'
        dbTypeText = {'rna':'Transcript','protein':'Protein'}[molType] +\
                     f' ({annotation})'

    
    evalue = '0.001'
    query = tempfile.NamedTemporaryFile(prefix='blastquery_', mode='w+t')
    query.writelines(seq)
    query.seek(0)
    out = tempfile.NamedTemporaryFile(prefix='blastresult_')

    cmd = [blastType, '-query', query.name, '-db', db, '-out', out.name, '-outfmt', '5', '-evalue', evalue]
    logger.debug("Running blast with query sequence:\n"+seq)
    logger.debug("Blast command:" + ' '.join(cmd))

    output = subprocess.run(cmd, capture_output=True)

    # TODO: catch errors from blast, e.g. if query sequence is invalid
    if( output.returncode != 0 ):
        logger.debug(f'Blast failed (returncode:{output.returncode}):\n{output.stderr}')
        raise Http404(f'Error occured when running blast!')

    result = NCBIXML.read(open(out.name))
    query.close()
    out.close()

    if(dbType != 'dna'):
        # For transcripts (or proteins)
        # get gene ID and description
        hit_seqs = [x.hit_def.split(' ')[0] for x in result.alignments]
        q = Transcripts.objects.filter(gff_id__in=hit_seqs, asm=assembly, ann=annotation)
        hit_genes = {tx.gff_id: tx.gene_id for tx in q}
        hit_transcripts = {tx.gff_id: tx.transcript_id for tx in q}
        unique_hit_genes = list(set(hit_genes.values()))
        q2 = Genes.objects.filter(gene_id__in=unique_hit_genes, asm=assembly, ann=annotation)
        gene_descriptions = {g.gene_id: g.description for g in q2}

    # For each hit, extract the values to display:
    hits = []
    for alignment in result.alignments:
        hit_seq_id = alignment.hit_def.split(' ')[0]
        for hsp in alignment.hsps:
            hit_stats={
                'score': hsp.score,
                'query_cov': f'{100*(hsp.query_end-hsp.query_start)/result.query_length:.0f}%',
                'e_value': f'{hsp.expect}',
                'perc_identity': f'{100*hsp.identities/hsp.align_length:.0f}%',
                'subject_length':alignment.length,
            }

            if(dbType != 'dna'): # transcripts/proteins
                hit_info={
                    'transcript_id': hit_transcripts[hit_seq_id],
                    'gene_id': hit_genes[hit_seq_id],
                    'description': gene_descriptions[hit_genes[hit_seq_id]],
                }
            else: # whole genome
                if (hsp.sbjct_start > hsp.sbjct_end): # reverse hit
                    hit_info={
                        'hit_locus': f'{hit_seq_id}:{hsp.sbjct_end}..{hsp.sbjct_start}',
                        'description': f'{hit_seq_id}:{hsp.sbjct_start}-{hsp.sbjct_end} (reverse)',
                    }
                else:
                    hit_info={
                        'hit_locus': f'{hit_seq_id}:{hsp.sbjct_start}..{hsp.sbjct_end}',
                        'description': f'{hit_seq_id}:{hsp.sbjct_start}-{hsp.sbjct_end}',
                    }

            hits.append({**hit_info,**hit_stats})



    context = {
        'blastType': blastType,
        'species': species,
        'assembly': assembly,
        'species_text': nameToText(species),
        'dbTypeText': dbTypeText,
        'nSubjects': len(result.alignments),
        'nHits': len(hits),
        'hits':  hits,
    }

    return render(request, 'blast/result.html', context)


