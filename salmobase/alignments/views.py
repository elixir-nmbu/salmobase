from django.http import JsonResponse, HttpResponse
from django.db.models import Q
from alignments.models import  PairwiseAlignment

# Create your views here.

def get_pairwise_alignment(request, alignment_name, qasm, tasm):
    tname = request.GET.get('tname', '')
    tstart = int(request.GET.get('tstart', 1))
    tend = int(request.GET.get('tend', 1))    
    format = request.GET.get('format', 'JSON')
    # alignment_name, tasm, qasm, tname, tstart, tend
    query = Q(alignment_name=alignment_name) & Q(qasm=qasm) & Q(tasm=tasm) & Q(tname=tname) & Q(tend__gte=tstart) & Q(tstart__lte=tend)
    alignments = PairwiseAlignment.objects.filter(query)
    # print(f'{alignment_name} ({qasm} vs {tasm}) {tname}:{tstart}-{tend}: {len(alignments)} hits (format: {format})' )

    
    if( format == 'PAF' ):
        # return alignments in PAF format (TSV with no header)
        # columns: "qname","qlen","qstart","qend","strand","tname","tlen","tstart","tend","matches","blocklen","mapq","cigar"
        # qlen, tlen, matches, blocklen, mapq are all ""
        tsv_data = ''
        for row in alignments:
            tsv_data += '\t'.join([row.qname, '', str(row.qstart), str(row.qend), row.strand, 
                                   row.tname, '', str(row.tstart), str(row.tend), '', '', '', row.cigar]) + '\n'

        # Return TSV response
        return HttpResponse(tsv_data, content_type='text/tab-separated-values')

    # Return JSON response
    data = list(alignments.values())
    return JsonResponse(data, safe=False)