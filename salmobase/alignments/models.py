from django.db import models

class PairwiseAlignment(models.Model):
    unique_id = models.IntegerField(primary_key=True)
    alignment_name = models.TextField()
    qasm = models.TextField()
    qname = models.TextField()
    qstart =  models.BigIntegerField()
    qend =  models.BigIntegerField()
    tasm = models.TextField()
    tname = models.TextField()
    tstart =  models.BigIntegerField()
    tend =  models.BigIntegerField()
    strand =  models.TextField()
    cigar =  models.TextField()
    class Meta:
        managed = False

class PairwiseAlignmentOverview(models.Model):
    alignment_name = models.TextField(primary_key=True)
    qasm = models.TextField()
    tasm = models.TextField()
    class Meta:
        managed = False