from django.urls import path
from . import views

app_name = 'alignments'

urlpatterns = [
    path('<str:alignment_name>/<str:qasm>/<str:tasm>', views.get_pairwise_alignment, name='get_pairwise_alignment'),
]