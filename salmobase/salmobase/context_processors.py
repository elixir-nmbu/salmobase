from django.conf import settings
from . import species
def google_analytics_id(request):
    return settings.GOOGLE_ANALYTICS

def all_species(request):
    return {
        'all_species': species.getAllSpecies()
    }


def searchFilterContext(request):
    '''Data needed to populate the search filter options'''
    return {
        'all_primary_assemblies': ','.join(species.getAllPrimaryAssemblies()),
        'all_annotated_assemblies': species.getAssembliesWithAnnotation()
    }