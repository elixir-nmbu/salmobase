from django.conf import settings
from salmobase.salmobaseConfigParser import SalmobaseConfig, SalmobaseConfigError

try:
  # load the salmobase definition
  salmobaseConfig = SalmobaseConfig(settings.SPECIES_YML)
except KeyError as e:
  raise SalmobaseConfigError("Missing: "+str(e))

# def listSpecies():
#     return([ species_meta[spc]["name"] for spc in species_meta ])

def isValidSpeciesName(name):
    speciesLower = [ species.lower() for species in salmobaseConfig.species ]
    return(name.lower() in speciesLower)

def nameToName(name):
    '''Get the case correct name given case insensitive name'''
    for species, meta in salmobaseConfig.species.items():
        if species.lower() == name.lower():
            return(species)
    raise NameError(f'"{name}" not in species list!')

def nameToSpc(name):
    '''Get the short form species name given case insensitive long name'''
    for species, meta in salmobaseConfig.species.items():
        if species.lower() == name.lower():
            return(meta.name_short)
    raise NameError(f'"{name}" not in species list!')

def spcToName(spc):
    '''Get the species name given short form name'''
    for species, meta in salmobaseConfig.species.items():
        if meta.name_short == spc:
            return(species)
    raise NameError(f'"{spc}" not in species list!')

def nameToText(name):
    '''Get the text formated species name given case insensitive name'''
    for species, meta in salmobaseConfig.species.items():
        if species.lower() == name.lower():
            return(meta.name_text)
    raise NameError(f'"{name}" not in species list!')

def nameToSci(name):
    '''Get the scientific species name given case insensitive name'''
    for species, meta in salmobaseConfig.species.items():
        if species.lower() == name.lower():
            return(meta.name_sci)
    raise NameError(f'"{name}" not in species list!')

def getPrimaryAssembly(name):
    return(salmobaseConfig.species[nameToName(name)].primary_assembly)

def getAllPrimaryAssemblies():
    return([v.primary_assembly for k,v in salmobaseConfig.species.items()])

def getAllAssemblyNames():
    assemblyNames = []
    for spc in salmobaseConfig.species.values():
        for asm in spc.assemblies.values():
            assemblyNames.append(asm.asm)
    return(assemblyNames)

def getAssembliesWithAnnotation():
    '''Per species, list assemblies excluding those without annotations'''
    # currently all assemblies have annotations though...
    retVal = {}
    for spc,species in salmobaseConfig.species.items():
        retVal[spc] = [asm for asm,assembly in species.assemblies.items() if len(assembly.annotations) > 0]
    return(retVal)

def getAssemblyNames(name):
    return(list(salmobaseConfig.species[nameToName(name)].assemblies))

def getSequenceSource(name, assembly):
    return(salmobaseConfig.species[nameToName(name)].assemblies[assembly].sequence['source'])

def getPrimaryAnnotation(name, assembly):
    return(salmobaseConfig.species[nameToName(name)].assemblies[assembly].primary_annotation)

def getAnnotations(name, assembly):
    return(salmobaseConfig.species[nameToName(name)].assemblies[assembly].annotations)

def getSpeciesDescription(name):
    return(salmobaseConfig.species[nameToName(name)].description)

def getAllSpecies():
    return(salmobaseConfig.species)

def getOrthologyByPrefix(og):
    '''Given an OG identifier, get the relevant orthology dataset meta-data'''
    for ortho, orthoDict in salmobaseConfig.configDict["orthology"].items():
        if og.startswith(orthoDict["prefix"]):
            return(ortho)
    # return None if no matching orthology found
    return None

def getOrthoSpeciesMeta(ortho):
    '''Get metadata per species for orthology dataset'''
    return salmobaseConfig.configDict["orthology"][ortho]["species"]
