from django.shortcuts import render

def home(request):
    return render(request, 'salmobase/home.html')

def tools(request):
    return render(request, 'salmobase/tools.html')

def resources(request):
    return render(request, 'salmobase/resources.html')