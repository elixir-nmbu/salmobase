// From: https://www.w3schools.com/howto/howto_js_autocomplete.asp
function autocomplete(input, output, search_url, click_url) {
  let currentFocus;
  let results = []
  input.addEventListener("input", function(e) {
    const val = this.value;
    let assembly = document.getElementById("assembly-filter").value
    let annotation = document.getElementById("annotation-filter").value
    closeAllLists();
    if (!val || val.length<3) { return false; }
    $.ajax({
      url: search_url,
      type: 'GET',
      data: { 'q': val, 'assembly': assembly, 'annotation': annotation, 'from': 1, 'to': 20, 'format': 'JSON' },
      async: false,
      success: function(response) {
        results =  JSON.parse(JSON.stringify(response.results)) 
      }
    })
    currentFocus = -1;
    const a = document.createElement("div");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "list-group autocomplete-list");
    output.appendChild(a);
    results.forEach(function(item) {
      const b = document.createElement("a");
      b.setAttribute("class", "list-group-item list-group-item-action autocomplete-item");
      b.setAttribute("href", `${click_url}id/${item['source']['gene_id']}?assembly=${item['source']['asm']}`);
      b.innerHTML += ''
        + '<div>'
          + ' <strong>' + item['highlight']['gene_name'] + "</strong>"
          + ' <small">' + item['highlight']['description'] + "</small>"
        + '</div>'
        + '<div>'
          + '<span class="badge rounded-pill bg-secondary">' + item['source']['ann'] +": "+ item['highlight']['gene_id'] + "</span>"
          + '<span class="badge rounded-pill bg-primary">' + item['source']['species'] + "</span>"
          + '<span class="badge rounded-pill bg-info">' + item['source']['asm'] + "</span>"
        + '</div>'
      a.appendChild(b);
    });
  });
  input.addEventListener("keydown", function(e) {
      if (currentFocus == -1 & e.which === 13 ) {
        // form.submit();
        return false;
      }
      let x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("a");
      if (e.key == "ArrowDown") {
        currentFocus++;
        addActive(x);
      } else if (e.key == "ArrowUp") {
        currentFocus--;
        addActive(x);
      } else if (e.key == "Enter") {
        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("active");
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("active");
    }
  }
  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-list");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != output) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  input.addEventListener("focus", (e) => {
    input.dispatchEvent(new Event('input'));
  })
  document.addEventListener("click", function (e) {
    if(e.target != input)
      closeAllLists(e.target);
  });
}