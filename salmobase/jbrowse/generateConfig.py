from django.conf import settings
from jbrowse.generateDatasetTracks import generateDatasetTracks
from salmobase.species import salmobaseConfig, getAllAssemblyNames
from jbrowse.generateOrthologSyntenyConfig import generateOrthologSyntenyTracks
from alignments.models import PairwiseAlignmentOverview

# generate JBrowse config


# Check if assembly has alias file
def hasAlias(assemblyName):

  asm = salmobaseConfig.assemblies[assemblyName]

  # Currently, alias files are only generated for genomes with NCBI annotations
  # or has genbank accession
  return( "NCBI" in asm.annotations or asm.sequence['accession'].startswith('GCA_') )



def assemblyConfig(assemblyName):
  """ Generate assembly config for given assembly """
  baseURL = settings.JBROWSE_BASE_URL
  
  asm = salmobaseConfig.assemblies[assemblyName]
  spc = asm.spc
  src = asm.sequence['source']

  path = f'/genomes/{spc}/{assemblyName}/sequence_{src}/{asm.getFastaBasename()}'

  # use filtered fai file?
  if( asm.hasFilteredFai() ):
    fai_filter = '.filtered'
  else:
    fai_filter = ''

  asmCfgDict = {
    'name': assemblyName,
    'sequence': {
      'type': 'ReferenceSequenceTrack',
      'trackId': f'{assemblyName}-refseqTrack',
      'adapter': {
        "type": "BgzipFastaAdapter",
        "fastaLocation": {
          "uri": f'{baseURL}{path}.gz',
          "locationType": "UriLocation"
        },
        "faiLocation": {
          "uri": f'{baseURL}{path}.gz{fai_filter}.fai',
          "locationType": "UriLocation"
        },
        "gziLocation": {
          "uri": f'{baseURL}{path}.gz.gzi',
          "locationType": "UriLocation"
        }
      },
    }, 
  }

  # add alias file if available
  if( hasAlias(assemblyName)):
    alias_filtered = fai_filter.replace(".", "_")
    alias_path = f'/genomes/{spc}/{assemblyName}/alias{alias_filtered}.txt'
    asmCfgDict["refNameAliases"] = {
        "adapter": {
          "type": "RefNameAliasAdapter",
          "location": {
            "uri": f'{baseURL}{alias_path}',
            "locationType": "UriLocation"
          }
        }
      }

  return(asmCfgDict)


# Tracks

def annotationTrackConfig(assemblyName):
  """ Generate annotation track config for given assembly """
  baseURL = settings.JBROWSE_BASE_URL

  asm = salmobaseConfig.assemblies[assemblyName]
  spc = asm.spc

  retList = []

  for ann, annObj in asm.annotations.items():
    path = f'/genomes/{spc}/{assemblyName}/annotations/{ann}/{annObj.getGffFileBaseName()}.gz'
    retList.append({
      'type': 'FeatureTrack',
      'trackId': f'{assemblyName}-{ann}-FeatureTrack',
      'name': f'Genes({ann})',
      'assemblyNames': [assemblyName],
      'category': ['Annotation'],
      'adapter': {
        'type': 'Gff3TabixAdapter',
        'gffGzLocation': { 
          'uri': f'{baseURL}{path}', 
          'locationType': 'UriLocation'},
        'index': {
          'location': { 
            'uri': f'{baseURL}{path}.tbi',
            'locationType': 'UriLocation'},          
        },
      }
    })
  
  return(retList)

def getAnnotationTrackIDs(assemblyName):
  """
  Get the trackID of the annotations available for given assembly.
  """
  trackCfg = annotationTrackConfig(assemblyName)
  
  return( [x['trackId'] for x in trackCfg] )


def alignmentTrackConfig():
  # check PairwiseAlignmentOverview table for available alignment datasets
  datasets = PairwiseAlignmentOverview.objects.all()

  # treat pairs of assemblies as unique
  datasets_uniq = []
  for d in datasets:
    x = {'alignment_name': d.alignment_name, 'assemblies': [d.qasm, d.tasm]}
    x['assemblies'].sort()
    if x not in datasets_uniq:  
      datasets_uniq.append(x)

  retList = []
  for d in datasets_uniq:
    name = d['alignment_name']
    assemblies = d['assemblies']
    trackName = f"alignments_{name}_{assemblies[0]}_{assemblies[1]}"
    
    retList.append({
      "type": "SyntenyTrack",
      "trackId": trackName,
      "name": trackName,
      "assemblyNames": assemblies,
      "category": [ "comparative", "alignments", name],
      "adapter": {
        "type": "PairwiseAlignmentAPIAdapter",
        "assemblyNames": assemblies,
        "baseAPIurl": f'/alignments/{name}'
      }
    })
  return(retList)
 
def generateConfig():
  """
  Generate config for all assemblies.
  """
  allAssemblies = getAllAssemblyNames()

  annTracks = [] 
  for asm in allAssemblies:
      annTracks.extend( annotationTrackConfig(asm) )

  comparativeTracks = generateOrthologSyntenyTracks()
  alignmentTracks = alignmentTrackConfig()

  (datasetTracks,datasetConnections) = generateDatasetTracks()

  plugins = [
    {
      'name': 'Mytest',
      'url': settings.STATIC_URL + 'jbrowse/js/jbrowse-plugin-mytest.umd.production.min.js'
    }]
  
  config = {
      'plugins': plugins,
      'assemblies':[ assemblyConfig(asm) for asm in allAssemblies ],
      'connections': datasetConnections,
      'tracks': annTracks + comparativeTracks + alignmentTracks + datasetTracks,
  }
  return(config)


def miniDefaultSessionConfig(assembly, location, annotation):
  asmCfg = assemblyConfig(assembly)

  annCfg = annotationTrackConfig(assembly)

  # add the relevant annotation track to deafault session
  annTrackId = f'{assembly}-{annotation}-FeatureTrack'
  
  defaultSession = {
    'name': 'JBrowse embedded session',
    'view': {
      'id': 'linearGenomeView',
      'type': 'LinearGenomeView',
      'hideHeader': True,
      'tracks': [
        {
          'type': 'FeatureTrack',
          'configuration': f'{annTrackId}',
          'displays': [
            {
              'type': 'LinearBasicDisplay',
              'configuration': f'{annTrackId}-LinearBasicDisplay',
              'height': 100,
              'mode': 'normal', # this does not seem to have any effect
            },
          ],
        }                
      ],
    },
  }

  config = {
    'assembly': asmCfg,
    'tracks': annCfg,
    'defaultSession': defaultSession,
    'location': location
  }
  return (config)