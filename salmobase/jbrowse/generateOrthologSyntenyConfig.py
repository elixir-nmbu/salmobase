from django.conf import settings
from salmobase.species import salmobaseConfig
from itertools import combinations

baseURL = settings.JBROWSE_BASE_URL

def syntenyTrackConfigTemplate(trackname, target_asm, query_asm, url, categories):
  return({
    "type": "SyntenyTrack",
    "trackId": f'{trackname}_track',
    "name": trackname,
    "assemblyNames": [target_asm, query_asm],
    "category": categories,
    "adapter": {
      "type": "PAFAdapter",
      "pafLocation": { "uri": url },
      "queryAssembly": query_asm,
      "targetAssembly": target_asm,
    }
  })



def generateOrthologSyntenyTrack(OGdataset, spcMeta, spc_1, spc_2):
  paraOrOrtho = 'orthologs'
  if spc_1 == spc_2:
    paraOrOrtho = 'paralogs'
    trackname = f'paralogs_{spcMeta[spc_1]["species"]}_{OGdataset}'
  else:
    paraOrOrtho = 'orthologs'
    trackname = f'orthologs_{spcMeta[spc_1]["species"]}_{spcMeta[spc_2]["species"]}_{OGdataset}'

  config = syntenyTrackConfigTemplate(
    trackname = trackname,
    target_asm = spcMeta[spc_2]['asm'],
    query_asm = spcMeta[spc_1]['asm'],
    url = baseURL + f'/comparative/orthologs/{OGdataset}/{paraOrOrtho}_{spc_1}_{spc_2}.paf.gz',
    categories = ["comparative","orthologs",OGdataset])
  return(config)

def generateOrthologSyntenyTracks(): 
  configList = []
  for OGdataset, OGmeta in salmobaseConfig.configDict['orthology'].items():
    # Rearrange the orthology species metadata to use 'name' as key
    spcMeta = {v['name']:{'species':k,'asm':v['asm']} for k, v in OGmeta['species'].items()}
    spcs = list(spcMeta)
    configList += [generateOrthologSyntenyTrack(OGdataset, spcMeta, spc, spc) for spc in spcs]
    spcs_pairs = list(combinations(spcs, 2))
    configList += [generateOrthologSyntenyTrack(OGdataset, spcMeta, x[0], x[1]) for x in spcs_pairs]
  return(configList)
