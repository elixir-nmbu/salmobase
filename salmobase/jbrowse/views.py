from django.http import JsonResponse
from django.shortcuts import render
from .generateConfig import generateConfig

def index(request):
    context = { 'jbrowse_url': '/datafiles/jbrowse/index.html' }
    return render(request, 'jbrowse/index.html', context)

def config_json(request):
    config = generateConfig()
    return JsonResponse(config, json_dumps_params={'indent': 2})
