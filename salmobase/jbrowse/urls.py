from django.urls import path
from . import views

app_name = 'jbrowse'

urlpatterns = [
    path('', views.index, name='index'),
    path('config.json', views.config_json),
]