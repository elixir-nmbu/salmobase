from django.conf import settings
import glob
import os
import yaml

# JBROWSE_BASE_URL contains the /datafiles prefix and must be removed
baseURL = settings.JBROWSE_BASE_URL.replace('/datafiles', '')

def bigwigTrackTemplate(id,name,assembly,url,categories):
  return({
    "type": "QuantitativeTrack",
    "trackId": id,
    "name": name,
    "assemblyNames": [assembly],
    "category": categories,
    "adapter": {
      "type": "BigWigAdapter",
      "bigWigLocation": {
        "locationType": "UriLocation",
        "uri": url
      }
    }
  })

def bedTrackTemplate(id,name,assembly,url,categories):
  return({
    "type": "FeatureTrack",
    "trackId": id,
    "name": name,
    "assemblyNames": [assembly],
    "category": categories,
    "adapter": {
      "type": "BedAdapter",
      "bedLocation": {
        "locationType": "UriLocation",
        "uri": url
      }
    }
  })

def generateMultiTracks(trackConfigEntry, datasetPath, datasetURL, datasetName, trackConfigFile):
  # validate trackConfigEntry
  if( not 'filelist' in trackConfigEntry ):
    raise(ValueError(f"{trackConfigFile} contains entry without 'filelist' key"))
  if( not 'assembly' in trackConfigEntry ):
    raise(ValueError(f"{trackConfigFile} contains entry without 'assembly' key"))
  tracks = []
  # get full path of filelist by adding directory of trackConfigFile
  filelistPath = os.path.join(datasetPath, trackConfigEntry['filelist'])
  # read lines in filelist as list
  with open(filelistPath, 'r') as f:
    fileList = f.read().splitlines()

  # get assembly
  assembly = trackConfigEntry['assembly']
  # get config
  config = trackConfigEntry['config']
  # Generate track for given config type
  for file in fileList:
    # get track id from filename
    trackId = os.path.basename(file)
    # get track name from filename without extension
    trackName = os.path.splitext(trackId)[0]
    # get categories from the path of the file
    categories = [datasetName] + os.path.dirname(file).split('/')
    url = datasetURL + '/' + file
    # generate track
    if config == "bigwig_default":
      tracks.append( bigwigTrackTemplate( trackId, trackName, assembly, url, categories) )
    elif config == "bed_default":
      tracks.append( bedTrackTemplate( trackId, trackName, assembly, url, categories) )
  return( tracks )

def generateTrackhubConnection(trackConfigEntry, trackConfigFile):
  # validate trackConfigEntry
  if( not 'url' in trackConfigEntry ):
    raise(ValueError(f"{trackConfigFile} contains trackhub entry without 'url' key"))
  if( not 'assemblies' in trackConfigEntry ):
    raise(ValueError(f"{trackConfigFile} contains trackhub entry without 'assemblies' key"))
  if( not 'name' in trackConfigEntry ):
    raise(ValueError(f"{trackConfigFile} contains trackhub entry without 'hub_name' key"))

  # generate connection
  return({
      "type": "UCSCTrackHubConnection",
      "connectionId": trackConfigEntry['name'],
      "name": trackConfigEntry['name'],
      "assemblyNames":  trackConfigEntry['assemblies'],
      "hubTxtLocation": {
        "uri": trackConfigEntry['url'],
        "locationType": "UriLocation"
      }
    })

def generateDatasetTracksFromConfigFile(trackConfigFile):
  """
  Parse track_config.yaml file and generate jbrowse track configuration.
  The track_config.yaml file allows for application of a single configuration to multiple tracks.
  The format of the track_config.yaml files is list of dictionaries
  with the following keys:
  { 
    'config': The type of config e.g. "bigwig_default", "bed_default", "trackhub"
    # if config == "bigwig_default" or "bed_default":
    'filelist': Path to file with list of track files. Relative to the track_config.yaml file
    'assembly': The assembly name e.g. "Ssal_v3.1"
    # if config == "trackhub":
    'url': The url to the trackhub hub.txt file
    'name': The name of the trackhub
    'assemblies': The list of assemblies in the trackhub
  }
  """
  # get base path of dataset
  datasetPath = os.path.dirname(trackConfigFile)
  datasetURL = baseURL + datasetPath
  # get name of dataset
  datasetName = os.path.basename(datasetPath)
  # read yaml file
  with open(trackConfigFile, 'r') as stream:
    try:
      trackConfig = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
      # TODO: log error and return(None)
      # For now re-throw error
      raise(exc)
  # validate trackConfig
  if( not isinstance(trackConfig, list) ):
    raise(ValueError(f"{trackConfigFile} should be a list!"))
  for trackConfigEntry in trackConfig:
    if( not isinstance(trackConfigEntry, dict) ):
      raise(ValueError(f"{trackConfigFile} contains non-dictionary element!"))
    if( not 'config' in trackConfigEntry ):
      raise(ValueError(f"{trackConfigFile} contains entry without 'config' key"))
  tracks = []
  connections = []
  # parse each trackConfigEntry
  for trackConfigEntry in trackConfig:
    if( trackConfigEntry['config'] in ['bigwig_default', 'bed_default'] ):
      tracks.extend(generateMultiTracks(trackConfigEntry, datasetPath, datasetURL, datasetName, trackConfigFile))
    elif( trackConfigEntry['config'] == 'trackhub' ):
      connections.append(generateTrackhubConnection(trackConfigEntry, trackConfigFile))
    else:
      raise(ValueError(f"{trackConfigFile} contains entry with unknown 'config' value"))

  return( tracks, connections )

def generateDatasetTracks():
  """
  Generate jbrowse tracks and connections for datasets

  Looks for /datafiles/datasets/*/.autoconfig.yaml files and generates jbrowse track 
  and connection configuration based on the metadata in those files. 
  """

  # scan for track_config.yaml files
  trackConfigFiles = glob.glob('/datafiles/datasets/*/.autoconfig.yaml')

  tracks = []
  connections = []
  # parse each track_config.yaml file and add to tracks
  for trackConfigFile in trackConfigFiles:
    (newTracks,newConnections) = generateDatasetTracksFromConfigFile(trackConfigFile)
    tracks.extend( newTracks )
    connections.extend( newConnections )

  return( tracks, connections )
