from django.shortcuts import get_list_or_404, render, get_object_or_404
from django.http import JsonResponse, Http404
from genes.models import Genes, OG, Genetrees, Xref
from genes.documents import GenesDocument
import re
from salmobase.species import getPrimaryAssembly, nameToSpc, getOrthologyByPrefix, getOrthoSpeciesMeta
from jbrowse.generateConfig import getAnnotationTrackIDs, miniDefaultSessionConfig
import operator
from functools import reduce
from elasticsearch_dsl.query import Q

def index(request):
    return render(request, 'genes/index.html')

# TODO: move these functions to other file
def getGeneIDtype(ann):
    if( ann == "EnsemblRapid"):
        return("Ensembl")
    else:
        return(ann)

def getGeneIDlink(ann, gene_id):
    if( ann == "NCBI"):
        return("https://www.ncbi.nlm.nih.gov/gene/"+gene_id)
    elif( ann == "Ensembl"):
        return("https://ensembl.org/id/"+gene_id)
    elif( ann == "EnsemblRapid"):
        return("https://rapid.ensembl.org/id/"+gene_id)
    else:
        return("")

# Prepare the context for the gene view
def getGeneContext(gene):
    context = {
        'gene': gene,
        'gene_id_type': getGeneIDtype(gene.ann),
        'gene_id_link': getGeneIDlink(gene.ann, gene.gene_id),
        'annotation_tracks': ",".join(getAnnotationTrackIDs(gene.asm)),
        'jbrowse_data': miniDefaultSessionConfig( 
            assembly = gene.asm,
            location = f'{gene.seqname}:{round(gene.start-(gene.end-gene.start)*0.25)}..{round(gene.end+(gene.end-gene.start)*0.25)}',
            annotation =  gene.ann,
        ),
        'xref': Xref.objects.filter(gene_id=gene.gene_id, asm=gene.asm, ann=gene.ann)
    }
    return context

# (Helper function) Deal with multiple hits for the gene view.
def thereCanOnlyBeOne(gene, assembly):
    if len(gene) > 1:
        # Do something if there are multiple genes

        # If gene is found in multiple assemblies, use primary assembly
        if( assembly == ''):
            # select the gene from primary assembly
            genePrimary = [x for x in gene if x.asm == getPrimaryAssembly(x.species)]
            # use primary assembly gene if there is one
            gene = genePrimary if len(genePrimary)>0 else gene
            # TODO: what if there are multiple non-primary assemblies?

        # TODO: Deal with multiple loci. E.g. select the first and 
        # produce a list of loci that can be selected from a dropdown.
    return gene[0]


def gene(request, gene_id):
    assembly = request.GET.get('assembly', '')
    if( assembly != '' ):
        gene = get_list_or_404(Genes.objects.filter(gene_id=gene_id, asm=assembly))
    else:
        gene = get_list_or_404(Genes.objects.filter(gene_id=gene_id))
    
    gene = thereCanOnlyBeOne(gene, assembly)
    context = getGeneContext(gene)

    return render(request, "genes/gene.html", context)

def geneByName(request, species, gene_name):
    assembly = request.GET.get('assembly', '')
    if( assembly != '' ):
        gene = get_list_or_404(Genes.objects.filter(gene_name=gene_name, asm=assembly))
    else:
        gene = get_list_or_404(Genes.objects.filter(gene_name=gene_name, species=species))
    
    # TODO: Deal with multiple annotations! I.e. select primary annotation
    
    gene = thereCanOnlyBeOne(gene, assembly)
    context = getGeneContext(gene)

    return render(request, "genes/gene.html", context)

def search(request):
    # elasticsearch-dsl docs:
    # https://elasticsearch-dsl.readthedocs.io/en/stable/api.html
    # https://github.com/elastic/elasticsearch-dsl-py/blob/master/examples/search_as_you_type.py
    q = request.GET.get('q', '')
    assembly = request.GET.get('assembly', '')
    if assembly != '':
        assembly = assembly.split(",")
    annotation = request.GET.get('annotation', '')
    if annotation != '':
        annotation = annotation.split(",")
    i_from = request.GET.get('from', 1)
    i_to = request.GET.get('to', 99)
    format = request.GET.get('format', 'html')

    s = GenesDocument.search()
    s = s.query("multi_match", query=q, type="bool_prefix",
        fields=['gene_id^100', 'gene_id._3gram', 'gene_name^10', 'gene_name._3gram', 'description', "description._3gram"])
    # Filter assembly
    if assembly != '':
        filters = [Q('match', asm = x) for x in assembly]
        s = s.filter(reduce(operator.ior, filters)) # asm1 OR asm2 OR ...
    # Filter annotation
    if annotation != '':
        filters = [Q('match', ann = x) for x in annotation]
        s = s.filter(reduce(operator.ior, filters))
    nhits = s.count()
    s = s.highlight_options(no_match_size=200, pre_tags='<mark>', post_tags='</mark>').highlight('gene_id', 'gene_name', 'description')
    hits = s[min(nhits,int(i_from)-1):min(nhits,int(i_to))]
    results = [{ 'source': hit.to_dict(), 'highlight': hit.meta.highlight.to_dict() } for hit in hits]
            
    context = { 
        'q': q,
        'filter': {'assembly': ",".join(assembly), 'annotation': ",".join(annotation)},
        'i_from' : i_from,
        'i_to' : i_to,
        'nhits' : nhits,
        'nshown' : len(results),
        'results':  results 
    }
    if format == 'JSON':
        return JsonResponse(context, safe=True)
    else:
        return render(request, 'genes/search.html', context)

def orthogroup(request):
    og = request.GET.get('og', None)
    genetree = get_object_or_404(Genetrees.objects.filter(og=og))
    # generate gene metadata dictionary with
    geneslist = list(Genes.objects.filter(og=genetree.og).values('gene_id', 'gene_name', 'species','asm'))
    genesMeta = {}
    for e in geneslist:
        # name equal the tip labels in tree
        tiplabel=e['gene_id'] + "_" + nameToSpc(e['species'])
        genesMeta[tiplabel] = {
            'gene_id': e['gene_id'],
            'displayName': f"{e['gene_name']} ({e['species']})",
            'url': f"id/{e['gene_id']}?assembly={e['asm']}"
        }
        
    # geneslist = [{**e, "label": e['gene_id']+"_"+nameToSpc(e['species'])} for  e in  geneslist]
    context = {
        'og': genetree.og,
        'genesMeta' : genesMeta,
        'cds_tree' : genetree.cdsTree
    }
    return JsonResponse(context, safe=True)

def synteny(request):
    '''
    Given an orthogroup ID (og) and target gene return the orthologs with neighboring genes
    '''
    # get request arguments
    og = request.GET.get('og', None)
    if( og is None ):
        raise Http404('Missing "og" parameter')  
    gene_id = request.GET.get('gene_id', None)
    if( og is None ):
        raise Http404('Missing gene_id parameter')  

    # identify which orthology dataset to use
    ortho = getOrthologyByPrefix(og)
    if( ortho is None ):
        raise Http404("orthogroup ID not recognized")  

    # get the species names (i.e. alias used in the OG table) and asm
    orthoMeta = getOrthoSpeciesMeta(ortho)
    availSpcs = [v["name"] for v in orthoMeta.values()]
    spc2asm = {v["name"]: v["asm"] for k,v in orthoMeta.items()}
    spc2species = {v["name"]: k for k,v in orthoMeta.items()}

    # get orthologs
    orthologs = list(OG.objects.filter(og=og, spc__in=availSpcs).values('gene_id', 'spc', 'og', 'teleost', 'salmonid'))

    # verify that target gene is among orthologs
    if not gene_id in [x["gene_id"] for x in orthologs]:
        raise Http404("gene_id not found in orthogroup")

    coord = [] # used to keep track of paralogs
    for o in orthologs:
        # Create an ortholog "id" {OG}.{teleost node}.{salmonid node}. This is
        # used to sort the orthologs by clade
        o['id'] = ''.join((o['og'], re.search('\..*', o['teleost']).group(), re.search('\..*', o['salmonid']).group()))
        
        # get position and chromosome of ortholog
        asm = spc2asm[o['spc']]
        gene_obj = get_list_or_404(Genes.objects.filter(gene_id=o['gene_id'], asm=asm))[0]
        
        coord = coord + [{'chromosome': gene_obj.seqname, 'position': gene_obj.position}]
        
        o['chromosome'] = gene_obj.seqname
        o['species'] = spc2species[o['spc']]
        
        # get the direction of the ortholog
        o['direction'] = 'forward' if gene_obj.strand == '+' else 'reverse'
        
        # get neighboring genes
        pos = gene_obj.position
        o['neighbours'] = list(Genes.objects.filter(asm=asm, seqname=gene_obj.seqname, position__lte=pos+10, position__gte=pos-10).values('position', 'gene_id', 'gene_name', 'description', 'strand', 'og'))
        
        for n in o['neighbours']:
            # store assembly for each gene so that we can link to it
            n['asm'] = asm
            # add chromosome and species to diplay as part of gene info
            n['chromosome'] = o['chromosome']
            n['species'] = o['species']
            # add display x position "idx" from 0 to 20 where the ortholog is 10. Flip order of neigbours
            n['idx'] = 10 + (n['position']-pos) * (-1 if o['direction'] == 'reverse' else 1)
            del n['position']
            # flip direction (strand) of each gene if ortholog direction is reversed
            if o['direction'] == 'reverse':
                n['strand'] = {'+':'-','-':'+'}[n['strand']]

    for o in orthologs:
        if o['gene_id'] == gene_id:
            target_gene_neighbours = o['neighbours']
    
    # define color classes [number from 1-21] for each OG in target gene neighbours
    i = 1; color_classes = {}
    for n in target_gene_neighbours:
        n['color_class'] = i
        if n['og'] != None:
            color_classes[n['og']] = i
        i += 1
    # apply color classes to all neighbours of all orthologs
    for o in orthologs:
        for n in o['neighbours']:
            if n['og'] in color_classes.keys():
                n['color_class'] = color_classes[n['og']]
            else:
                n['color_class'] = 0

    
    if (len(orthologs)>1): #check if there're tandem genes and keep only one
        remove_index = []
        for i in range(0,len(orthologs)-2):
            for j in range(i+1,len(orthologs)-1):
                if (coord[i]['chromosome'] == coord[j]['chromosome']) and abs(coord[i]['position'] - coord[j]['position']) <= 10:
                    if (orthologs[j]['gene_id'] == gene_id):
                        #remove i-th element in orthologs as a tandem gene to j-th element
                        remove_index = remove_index + [i]
                    else:
                        #remove j-th element in orthologs as a tandem gene to j-th element
                        remove_index = remove_index + [j]
        orthologs = [o for i,o in enumerate(orthologs) if i not in remove_index]
    
    # Sort orthologs using created id (i.e. by teleost and salmonid clades)
    def getId(elem):
        return elem['id']
    orthologs.sort(key=getId)
    # Return orthologs and their neighbour data as reponse
    context = { 'orthologs': orthologs }
    return JsonResponse(context, safe=True)