from django.db import models

class Genes(models.Model):
    id = models.BigIntegerField(primary_key=True) # Note: unique number for each row in database.
    species = models.TextField()
    ann = models.TextField()
    asm = models.TextField()
    seqname = models.TextField()
    start = models.BigIntegerField()
    end = models.BigIntegerField()
    strand = models.TextField()
    gene_id = models.TextField()
    version = models.BigIntegerField(blank=True, null=True) # Ensembl gene version
    gene_name = models.TextField(blank=True)
    biotype = models.TextField()
    description = models.TextField(blank=True)
    og = models.TextField(blank=True) # ortholog group
    position = models.BigIntegerField() # the Nth gene along chromosome
    class Meta:
        managed = False

# gene ID conversion between different annotations of same assembly
class Xref(models.Model):
    species = models.TextField()
    asm = models.TextField()
    ann = models.TextField()
    gene_id = models.TextField(primary_key=True)
    ann_other = models.TextField()
    gene_id_other = models.TextField()
    class Meta:
        managed = False

class Transcripts(models.Model):
    gff_id = models.TextField(primary_key=True)
    species = models.TextField()
    ann = models.TextField()
    asm = models.TextField()
    seqname = models.TextField()
    start = models.BigIntegerField()
    end = models.BigIntegerField()
    feature = models.TextField()
    gene_id = models.TextField(blank=True)
    transcript_id = models.TextField(blank=True)
    class Meta:
        managed = False

class OG(models.Model):
    gene_id = models.TextField(primary_key=True)
    og = models.TextField()
    spc = models.TextField()
    salmonid = models.TextField(blank=True, null=True)
    teleost = models.TextField(blank=True, null=True)
    class Meta:
        managed = False

class Genetrees(models.Model):
    og = models.TextField(primary_key=True)
    cdsTree = models.TextField()

    class Meta:
        managed = False