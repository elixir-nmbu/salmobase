from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import SearchAsYouType
from .models import Genes

@registry.register_document
class GenesDocument(Document):
    name = SearchAsYouType(max_shingle_size=3)

    class Index:
        name = "genes"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Genes # The model associated with this Document
        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'gene_name',
            'gene_id',
            'description',
            'seqname',
            'start',
            'end',
            'strand',
            'species',
            'ann',
            'asm'
        ]

