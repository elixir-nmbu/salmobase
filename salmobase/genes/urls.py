from django.urls import path
from . import views

app_name = 'genes'

urlpatterns = [
    path('', views.index, name='index'),
    path('id/<str:gene_id>', views.gene, name='gene'),
    path('<str:species>/<str:gene_name>', views.geneByName, name='geneByName'),
    path('search', views.search, name='search'),
    path('orthogroup', views.orthogroup, name='orthogroup'),
    path('synteny', views.synteny, name='synteny'),
]