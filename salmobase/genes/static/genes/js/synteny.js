
function draw_synteny(div_name, data, target_gene, gene_url) {
    
    var L = data.length
    var N = 21 //data[0].neighbours.length
    var W = 600
    var chrW = W - 300
    var chrH = 15
    var H = chrH * L 
    var colors = [
        'white', '#9b0005',
        '#f80c12', '#ee1100', '#ff3311',
        '#ff4422', '#ff6644', '#ff9933',
        '#feae2d', '#ccbb33', '#d0c310',
        '#aacc22', '#69d025', '#22ccaa',
        '#12bdb9', '#11aabb', '#4444dd',
        '#3311bb', '#3b0cbd', '#442299',
        '#3e1172', '#5a0c64'
    ]
    // colors = ['white']
    // frequency = 5/N
    // for (var i = 1; i <= N; ++i) {
    //     r = Math.floor(Math.sin(frequency*i + 0) * (127) + 128);
    //     g = Math.floor(Math.sin(frequency*i + 2) * (127) + 128);
    //     b = Math.floor(Math.sin(frequency*i + 4) * (127) + 128);
    //     colors[i] = 'rgb('+r+','+g+','+b+')'
    // }
    // Syteny svg container
    var svg = d3.select(div_name)
        .append('svg')
        .attr('class', 'synteny')
        .attr('viewBox', [0, 0, W, H].join(' '))
        .attr('preserveAspectRatio', 'xMidYMid meet')

    // Tooltip div
    d3.select(div_name)
        .append('div')
        .attr('class', 'tooltip')
        .on('mouseover', function (d, i) { 
            d3.selectAll('.tootip')
                .style('opacity', 0) 
        })

    // Chromosome svg containers
    var chr = svg.selectAll('.chr')
        .data(data)
        .enter()
        .append('svg')
        .attr('class', 'chr')
        // .attr('id', function(d) { return d.species + d.gene_id })
        .attr('x', 0)
        .attr('y', function(d, i) { return d3.range(0, H, chrH)[i] })
        .attr('height', chrH)
        .attr('width', '100%')
    
    // Chromosome name
    chr.append('text')
        //.text(function(d) { return d.gene_id+' '+d.species+' [N3:'+d.N3.match(/\.(.*)/)[1]+' N9:'+d.N9.match(/\.(.*)/)[1]+']' })
        //.text(function(d) { return d.gene_id+' '+d.species+' '+d.chromosome+' '+d.direction+' strand ['+d.id+']' })
        .text(function(d) { return d.species+' '+d.chromosome+' '+d.direction+' strand'})
        .attr('x', chrW+5)
        .attr('y', chrH)
        .style('font-size', '6px')
        .attr('dy', '-6px')
        .style('font-weight', function(d){
            if (d.gene_id == target_gene) return 'bold'
            else return 'regular'
        })

    // Chromosome background
    var chrP = 0.1 // Padding between chromosomes
    chr.append('rect')
        .attr('width', chrW)
        .attr('height', chrH * (1-chrP*2))
        .attr('y', (chrH * chrP))
        .attr('stroke', 'black')
        .attr('stroke-width', 0.5)
        .attr('fill', 'grey')
        .attr('fill-opacity', function(d){
            if (d.gene_id == target_gene) return 0.5
            else return 0.1
        })

    // Chromosome midline
    chr.append('rect')
        .attr('width', '2px')
        .attr('height', '100%')
        .attr('fill', 'grey')
        .attr('y', 0)
        .attr('x', chrW/2)

    // Gene groups
    var g = chr.selectAll('svg')
        .data(function(d) { return d.neighbours })
        .enter()
        .append('g')
    
    // Gene drawing
    g.append('path')
        .attr('id', function(d) { return d.gene_name })
        .attr('class', function(d) { if (d.og == null) { return 'gene ' + d.gene_name } else { return 'gene ' + d.og } })
        .attr('d', function(d, i) {
            px = 0.1 // padding x
            py = chrP * 2 // padding y
            w = chrW/N * (1 - px*2)
            h = chrH * (1 - py*2)
            s = 1
            x = d3.range(0, chrW, chrW/N)[d.idx] + (chrW/N * px)
            if (d.strand == '-' ) { x += w; s = -1 }
            y = 0 + (chrH * py)
            a = 1/3 // point proportion
            path = [
                'M '+(x)+','+(y),
                'l '+(s*w*(1-a))+','+(0),
                'l '+(s*w*a)+','+(h/2),
                'l '+(-1*s*w*a)+','+(h/2),
                'l '+(-1*s*w*(1-a))+','+(0),
                'Z'
            ].join(' ')
            return path
        })
        .attr('fill', function(d) { return colors[d.color_class] })
        .attr('stroke', 'black')
        .attr('stroke-width', 1)
        .attr('opacity', 1)

    // Gene selection area
    g.append('rect')
        .attr('id', function(d) { return d.gene_name })
        .attr('class', function(d) { return d.og })
        .attr('info', function(d) { return d.gene_id+'| '+d.gene_name+': '+d.description+' <em>'+d.species+'</em> '+d.chromosome })
        .attr('url', function(d) { return d.gene_id+'?assembly='+d.asm })
        .attr('x', function(d, i) { return d3.range(0, chrW, chrW/N)[d.idx] })
        .attr('y', 0)
        .attr('width', (chrW/N))
        .attr('height', chrH)
        .attr('opacity', 0)
        .on('mouseover', handleMouseOverGene)
        .on('mouseout', handleMouseOutGene)
        .on('click', handleMouseClickGene)

    function handleMouseOverGene(d, i) {
        d3.selectAll('.gene:not(.'+this.getAttribute('class')+')')
            .transition().duration(00)
            .attr('opacity', 0.3)
        d3.selectAll('.gene .'+this.getAttribute('class'), ', #'+this.getAttribute('id'))
            .attr('opacity', 1)
        d3.select('.tooltip')
            .html(this.getAttribute('info'))
            .transition()
            .duration(200)
            .style('opacity', 1)
    }
 
    function handleMouseOutGene(d, i) {
        d3.selectAll('.gene')
            .transition().duration(100)
            .attr('opacity', 1)
        d3.selectAll('.tooltip')
            .transition()
            .duration(500)		
            .style('opacity', 0)
            
    }

    function handleMouseClickGene(d, i) {
        window.location.assign(gene_url + this.getAttribute('url'))
    }
}