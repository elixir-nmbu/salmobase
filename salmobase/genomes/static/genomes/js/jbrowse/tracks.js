export default [{
    type: 'FeatureTrack',
    trackId: 'Genes-FeatureTrack',
    name: 'Genes',
    assemblyNames: ['Genome'],
    category: ['Annotation'],
    adapter: {
        type: 'Gff3TabixAdapter',
        gffGzLocation: {
        uri:
        source+'.gff.gz',
        },
        index: {
        location: {
            uri:
            source+'.gff.gz.tbi',
        },
        },
    },
}]