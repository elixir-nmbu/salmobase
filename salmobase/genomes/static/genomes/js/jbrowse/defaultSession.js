
export default {
    name: 'My session',
    view: {
      id: 'linearGenomeView',
      type: 'LinearGenomeView',
      // hideControls: false ,
      // hideHeader: true ,
      tracks: [
        {
          type: 'ReferenceSequenceTrack',
          configuration: 'Genome-ReferenceSequenceTrack',
          displays: [
            {
              type: 'LinearReferenceSequenceDisplay',
              configuration:
                'Genome-ReferenceSequenceTrack-LinearReferenceSequenceDisplay',
            },
          ],
        },
        {
          type: 'FeatureTrack',
          configuration: 'Genes-FeatureTrack',
          displays: [
            {
              type: 'LinearBasicDisplay',
              configuration: 'Genes-FeatureTrack-LinearBasicDisplay',
            },
          ],
        },
      ],
    },
  }