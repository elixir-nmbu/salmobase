
export default {
  name: 'Genome',
  sequence: {
      type: 'ReferenceSequenceTrack',
      trackId: 'Genome-ReferenceSequenceTrack',
      adapter: {
      type: 'BgzipFastaAdapter',
      fastaLocation: {
          uri:
          source+'.fna.gz',
      },
      faiLocation: {
          uri:
          source+'.fna.gz.fai',
      },
      gziLocation: {
          uri:
          source+'.fna.gz.gzi',
      },
      },
  },
}