
const { createViewState, JBrowseLinearGenomeView } = JBrowseReactLinearGenomeView
const { createElement } = React
const { render } = ReactDOM


// parse the jbrowse config passed from django as JSON
const data = JSON.parse(document.getElementById('jbrowse_data').textContent)

let state;

const myOnChange = patch => {
  localStorage.setItem(data.autosave,JSON.stringify(state.session.view))
  sessionStorage.setItem(data.autosave,JSON.stringify(state.session.view))
  console.log("onChange: saving state...")
}

const loadViewState = (autosaveID) => {
  const sessionStoredViewState = sessionStorage.getItem(autosaveID)
  if(sessionStoredViewState){
    console.log("Loading view state from sessionStorage")
    return( JSON.parse(sessionStoredViewState) )
  }

  const localStoredViewState = localStorage.getItem(autosaveID)
  if(localStoredViewState){
    console.log("Loading view state from localStorage")
    return( JSON.parse(localStoredViewState) )
  }

  return
}

loadSession = (autosaveID, defaultSession) => {
  console.log('Autosave ID: '+ autosaveID)
  const savedSessionView = loadViewState(autosaveID)

  if(savedSessionView){
    defaultSession.view = savedSessionView
    return(defaultSession)
  }

  console.log('No autosave found. Using default.')
  return(defaultSession)
}

if( data.autosave ){
  state = createViewState({
    assembly: data.assembly,
    tracks: data.tracks,
    location: data.location,
    defaultSession: loadSession(data.autosave, data.defaultSession),
    onChange: myOnChange,
  })
} else {
  state = createViewState({
    assembly: data.assembly,
    tracks: data.tracks,
    location: data.location,
    defaultSession: data.defaultSession,
  })
}

// render the react jbrowse component
render(
  createElement(JBrowseLinearGenomeView, { viewState: state }),
  document.getElementById('jbrowse')
  )


          