from django.db import models

# Create your models here.

class Chromosomes(models.Model):
    name = models.TextField()
    seqname = models.TextField(primary_key=True)
    ensembl_name = models.TextField()
    refseq_accn = models.TextField()
    genbank_accn = models.TextField()
    sequence_name = models.TextField()
    is_chromosome = models.BooleanField()
    length =  models.BigIntegerField()
    spc = models.TextField()
    asm = models.TextField()
    class Meta:
        managed = False

class Syntenies(models.Model):
    id = models.IntegerField(primary_key=True)
    chromosome_x = models.TextField()
    begin_x =  models.BigIntegerField()
    end_x =  models.BigIntegerField()
    chromosome_y = models.TextField()
    begin_y =  models.BigIntegerField()
    end_y =  models.BigIntegerField()
    number_of_anchorpoints = models.IntegerField()
    aore_count = models.IntegerField()
    lore_count = models.IntegerField()
    color = models.TextField()
    orthology = models.TextField()
    spc = models.TextField()
    class Meta:
        managed = False

class Bands(models.Model):
    id = models.IntegerField(primary_key=True)
    chrom = models.TextField()
    start = models.BigIntegerField()
    end = models.BigIntegerField()
    color = models.TextField()
    orthology = models.TextField()
    spc = models.TextField()
    class Meta:
        managed = False