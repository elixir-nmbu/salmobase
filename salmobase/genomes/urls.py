from django.urls import path
from . import views

app_name = 'genomes'

urlpatterns = [
    path('<str:species>/<str:assembly>/overview', views.overview, name='overview'),
]