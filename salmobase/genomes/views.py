from django.http import Http404
from django.urls import reverse
from django.shortcuts import render
from salmobase.species import isValidSpeciesName, nameToText, nameToSci, getPrimaryAssembly, getAssemblyNames, getSpeciesDescription, salmobaseConfig
from django.conf import settings
from genomes.models import Chromosomes,Syntenies,Bands
from jbrowse.generateConfig import getAnnotationTrackIDs


def overview(request, species, assembly):
    if( not isValidSpeciesName(species)):
        raise Http404(f'"{species}" is not a valid species name')
    if( assembly == '' ):
        assembly = getPrimaryAssembly(species)
    else:
        if( not assembly in getAssemblyNames(species)):
            raise Http404(f'"{assembly}" is not a valid assembly name for {species}')

    context = {
        'species': species,
        'species_text': nameToText(species),
        'species_sci': nameToSci(species),
        'description': getSpeciesDescription(species),
        'chromosomes': get_chromosome_data(species, assembly),
        'syntenies': get_synteny_data(species,assembly),
        'bands': get_band_data(species,assembly),
        'BASE_URL': settings.JBROWSE_BASE_URL,
        'assembly': salmobaseConfig.species[species].assemblies[assembly],
        'orthology': get_orthology(species, assembly)
    }
    return render(request, "genomes/overview.html", context)

def get_chromosome_data(species, assembly):
    queryset = Chromosomes.objects.filter(spc=species, asm=assembly,is_chromosome=True)
    chromosomes = [{"chromosome": x.name, 
                    "length": x.length,
                    "ID":x.seqname,
                    "refseq_accn":x.refseq_accn,
                    "genbank_accn":x.genbank_accn,
                    "sequence_name":x.sequence_name,
                    "link":reverse('jbrowse:index')+
                      f'?assembly={assembly}&loc={x.seqname}&tracks={",".join(getAnnotationTrackIDs(assembly))}'
                    } for x in queryset]
    # Sort by refseq_accn if it is available
    if( not chromosomes[0]['refseq_accn'] is None):
        chromosomes.sort(key=lambda x: x["refseq_accn"])
    return(chromosomes)

def get_orthology(species, assembly):
    for ortho,orthoDict in salmobaseConfig.configDict['orthology'].items():
        for spc,spcDict in orthoDict['species'].items():
            if (spc==species and spcDict['asm']==assembly):
                return(ortho) #return the first orthology
    return([])

def get_synteny_data(species, assembly):
    queryset = Syntenies.objects.filter(spc=species, orthology = get_orthology(species, assembly))
    return([{"chromosome_x": x.chromosome_x,
            "begin_x": x.begin_x,
            "end_x": x.end_x,
            "chromosome_y": x.chromosome_y,
            "begin_y": x.begin_y,
            "end_y": x.end_y,
            "number_of_anchorpoints": x.number_of_anchorpoints,
            "lore_count": x.lore_count,
            "aore_count": x.aore_count,
            "color": x.color} for x in queryset])

def get_band_data(species, assembly):
    queryset = Bands.objects.filter(spc=species, orthology = get_orthology(species, assembly))
    return([{"chrom": x.chrom,
            "chromStart": x.start,
            "chromEnd": x.end,
            "color": x.color} for x in queryset])
