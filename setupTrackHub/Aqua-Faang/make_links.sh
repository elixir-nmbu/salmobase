# Generate links to the relevant track files under ../nfcore
# The purpose is to make the directory structure more simpler
# when selecting tracks in JBrowse

for SPC in {AtlanticSalmon,RainbowTrout}; do
  for MAP in {BodyMap,DevMap}; do
    for ASSAY_TYPE in {ATAC,RNA,ChIP}; do
      if [ "$MAP" = "BodyMap" ]; then
        # include the nf-core result dir for all the tissues
        RESULTS_DIR=$SPC/$MAP/$ASSAY_TYPE/*/results
      else
        RESULTS_DIR=$SPC/$MAP/$ASSAY_TYPE/results
      fi
      if [ "$ASSAY_TYPE" = "RNA" ]; then
        BAM_DIR=star_salmon
      else
        BAM_DIR=bwa/mergedLibrary
      fi
      mkdir -p $SPC/$MAP/$ASSAY_TYPE/bigWig
      cd $SPC/$MAP/$ASSAY_TYPE/bigWig
      eval "ln -s ../../../../../nfcore/$RESULTS_DIR/$BAM_DIR/bigwig/*.bigWig ."
      cd -
      mkdir -p $SPC/$MAP/$ASSAY_TYPE/bam
      cd $SPC/$MAP/$ASSAY_TYPE/bam
      eval "ln -s ../../../../../nfcore/$RESULTS_DIR/$BAM_DIR/*bam* ."
      cd -
      if [ "$ASSAY_TYPE" != "RNA" ]; then
        mkdir -p $SPC/$MAP/$ASSAY_TYPE/narrowPeak
        cd $SPC/$MAP/$ASSAY_TYPE/narrowPeak
        eval "ln -s ../../../../../nfcore/$RESULTS_DIR/$BAM_DIR/macs/narrowPeak/*narrowPeak ."
        cd -
      fi
      if [ "$ASSAY_TYPE" = "ChIP" ]; then
        mkdir -p $SPC/$MAP/$ASSAY_TYPE/broadPeak
        cd $SPC/$MAP/$ASSAY_TYPE/broadPeak
        eval "ln -s ../../../../../nfcore/$RESULTS_DIR/$BAM_DIR/macs/broadPeak/*broadPeak ."
        cd -
      fi
    done
  done
done
