#!/bin/bash
set -e

# Setting up the trackhub files for the Aqua-Faang tracks

destdir=../datafiles/datasets/Aqua-Faang

# Assuming that the nfcore output is stored in the $destdir/nfcore

# copy the premade hub files
cp -r Aqua-Faang/trackhub $destdir

# generate links to track files with reorganised directory structure
# note: needs to be in the destination directory
make_links=$(pwd)/Aqua-Faang/make_links.sh
(cd $destdir/trackhub; bash $make_links)

# Generate bigbeds

# get chrom.sizes from .fai
cut -f 1,2 ../datafiles/genomes/RainbowTrout/USDA_OmykA_1.1/sequence_Ensembl/Oncorhynchus_mykiss.USDA_OmykA_1.1.dna_sm.toplevel.fa.gz.fai > Omyk.chrom.sizes
cut -f 1,2 ../datafiles/genomes/AtlanticSalmon/Ssal_v3.1/sequence_Ensembl/Salmo_salar.Ssal_v3.1.dna_sm.toplevel.fa.gz.fai > Ssal.chrom.sizes

for broadPeak_file in $(find $destdir/trackhub/RainbowTrout -name "*.broadPeak"); do
    echo "Created BigBed file $broadPeak_file.bb"
    # maximum 1000 in score column 
    awk -F '\t' '{for (i=1; i<=NF; i++) printf "%s%s", (i == 5 ? ($i < 1000 ? $i : 1000) : $i), (i==NF?"\n":"\t")}' $broadPeak_file > $broadPeak_file.tmp
    bedToBigBed -type=bed6+3 -as=bigBroadPeak.as \
      $broadPeak_file.tmp\
      Omyk.chrom.sizes $broadPeak_file.bb
    # remove temp files
    rm $broadPeak_file.tmp
done

for narrowPeak_file in $(find $destdir/trackhub/RainbowTrout -name "*.narrowPeak"); do
    echo "Created BigBed file ${narrowPeak_file%.narrowPeak}.bigNarrowPeak"
    # maximum 1000 in score column 
    awk -F '\t' '{for (i=1; i<=NF; i++) printf "%s%s", (i == 5 ? ($i < 1000 ? $i : 1000) : $i), (i==NF?"\n":"\t")}' $narrowPeak_file > $narrowPeak_file.tmp
    bedToBigBed -type=bed6+4 -as=bigNarrowPeak.as \
      $narrowPeak_file.tmp\
      Omyk.chrom.sizes ${narrowPeak_file%.narrowPeak}.bigNarrowPeak
    # remove temp files
    rm $narrowPeak_file.tmp
done

# repeat for salmon
for broadPeak_file in $(find $destdir/trackhub/AtlanticSalmon -name "*.broadPeak"); do
    echo "Created BigBed file $broadPeak_file.bb"
    # maximum 1000 in score column 
    awk -F '\t' '{for (i=1; i<=NF; i++) printf "%s%s", (i == 5 ? ($i < 1000 ? $i : 1000) : $i), (i==NF?"\n":"\t")}' $broadPeak_file > $broadPeak_file.tmp
    bedToBigBed -type=bed6+3 -as=bigBroadPeak.as \
      $broadPeak_file.tmp\
      Ssal.chrom.sizes $broadPeak_file.bb
    # remove temp files
    rm $broadPeak_file.tmp
done

for narrowPeak_file in $(find $destdir/trackhub/AtlanticSalmon -name "*.narrowPeak"); do
    echo "Created BigBed file ${narrowPeak_file%.narrowPeak}.bigNarrowPeak"
    # maximum 1000 in score column 
    awk -F '\t' '{for (i=1; i<=NF; i++) printf "%s%s", (i == 5 ? ($i < 1000 ? $i : 1000) : $i), (i==NF?"\n":"\t")}' $narrowPeak_file > $narrowPeak_file.tmp
    bedToBigBed -type=bed6+4 -as=bigNarrowPeak.as \
      $narrowPeak_file.tmp\
      Ssal.chrom.sizes ${narrowPeak_file%.narrowPeak}.bigNarrowPeak
    # remove temp files
    rm $narrowPeak_file.tmp
done

# remove chrom.sizes
rm Omyk.chrom.sizes
rm Ssal.chrom.sizes

# generate trackDb files
(cd $destdir/trackhub; find RainbowTrout \( -iname \*.bigWig -o -iname \*.bam -o -iname \*.bb -o -iname \*.bigNarrowPeak \) ) | sort |\
  python makeTrackDb.py > $destdir/trackhub/RainbowTrout_trackDb.txt

(cd $destdir/trackhub; find AtlanticSalmon \( -iname \*.bigWig -o -iname \*.bam -o -iname \*.bb -o -iname \*.bigNarrowPeak \) ) | sort |\
  python makeTrackDb.py > $destdir/trackhub/AtlanticSalmon_trackDb.txt

