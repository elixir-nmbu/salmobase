# Generate trackhub trackDb files
#
# The input is list of files.
# The output is a trackDb file.
#
# Tracktype is inferred from etension of file.
# Track supertrack hierarchy is same as directory structure.
#

import os
import sys

def trackDbTemplate(path, trackID, trackName,trackType):
  # type is bigNarrowPeak or bigWig
  # parent is the directory with / replaced by -
  parent = os.path.dirname(path).replace("/", "-")
  return(f"""track {trackID}
bigDataUrl {path}
shortLabel {trackName}
type {trackType}
parent {parent}
""")

def superTrackTemplate(directory):
  parent = os.path.dirname(directory).replace("/", "-")
  return("\n".join([
    f"track {directory.replace('/', '-')}",
    f"shortLabel {os.path.basename(directory)}",
    f"longLabel",
    f"superTrack on show",
    f"parent {parent}\n" if parent != "" else ""
  ]))

def generateSuperTracks(filelist):
  """
  Get all unique directories from filelist and generate list of super-tracks
  """
  directories = set([os.path.dirname(f) for f in filelist])
  # get all parent directories
  for directory in directories.copy():
    parent = os.path.dirname(directory)
    while parent != "":
      directories.add(parent)
      parent = os.path.dirname(parent)
  return([superTrackTemplate(dir) for dir in directories])

def generateTracks(filelist):
  tracks=[]
  for f in filelist:
    trackID = os.path.basename(f)
    trackName = os.path.splitext(trackID)[0] # remove extension
    # get track type from extension
    if f.endswith(".bigWig"):
      trackType = "bigWig"
    elif f.endswith(".bam"):
      trackType = "bam"
    elif f.endswith(".bb"):
      trackType = "bigBed"
    elif f.endswith(".bigNarrowPeak"):
      trackType = "bigNarrowPeak"
    else:
      print(f"Unknown file extension: {f}")
      sys.exit(1)
    tracks.append(trackDbTemplate(f, trackID, trackName,trackType))
  return(tracks)

def main():
  # get filelist from stdin
  filelist = sys.stdin.readlines()
  filelist = [f.strip() for f in filelist]
  # generate supertracks and tracks
  tracks = "\n".join(generateSuperTracks(filelist) + generateTracks(filelist))
  # output to stdout
  print(tracks)

if __name__ == "__main__":
  main()
