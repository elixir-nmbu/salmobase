# Change Log

## [Spring2023] - 2023-04-11

- Added: Comparative genome alignments for salmon and trout
- Added: Comparative ortholog/paralog tracks
- Added: TrackHub for Aqua-Faang tracks
- Maintenence: logrotate added
- Fix: Genomes without RefSeq annotation now shows chromosome plot
- Genome browser is now using JBrowse2 web. Embedded browser is only used in the gene page.
- "Genome browser" removed from menu. The genome browser can now be opened from the genome page by clicking on a chromosome or by select JBrowse from the tools menu.
- Link to download choromosomes.tsv on genome page.
- Jbrowse config now generated dynamically

## [NewYear2023] - 2023-01-02

### Added

- New assemblies: 
- - Atlantic salmon: Ssal_Brian_v1.0
- - Atlantic salmon: Ssal_ALTA
- Gene page now shows link between Ensembl and NCBI annotation

## Correction 2022-12-05

The assembly that was originally submitted as Arctic char (Salvelinus alpinus) has been corrected to Dolly Varden (Salvelinus malma).
            
## [Nov2022] - 2022-11-29

Major changes mainly to allow multiple assemblies and annotaions per species.

### Added

- Support for multiple assemblies per species (old assemblies are still there)
- New assemblies: 
- - Atlantic salmon: Ssal_v3.1
- - Rainbow trout: USDA_OmykA_1.1
- - Coho salmon: Okis_V2
- Support both NCBI and Ensembl annotations.
- Filter for gene search: Search selected species, assembly or annotation.
- New gene trees and ortholog groups for the new species based on Ensembl annotations
- Chromosome alias: Now possible to load tracks with alternative sequence names. Valid sequence IDs are. refseq, genbank, Ensembl or “sequence name”. Aliases are also displayed when hovering the chromosome on the genome page.
- Gene page URL now uses geneIDs by default (NCBI gene ID or Ensembl gene ID). Gene annotations from earlier assemblies can be accessed by specifying the assembly with a parameter. E.g. https://salmobase.org/genes/id/100306804?assembly=ICSASG_v2 
